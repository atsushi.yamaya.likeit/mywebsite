<%@	page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>LessonUpdate</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/btn.css">
<link rel="stylesheet" href="css/footer.css">
<link rel="stylesheet" href="css/font.css">
</head>
<header>
	${university.name} ${student.name}

	<div align="right">
		<font size="3"> <a href="Logout" class="Logout">ログアウト</a>
		</font>
		<form action="USTimeTable" method="post">
			<input type="hidden" name="StuID" value="${student.id}"> <input
				type="hidden" name="UniID" value="${university.id }">
			<button type="submit" class="btn-Update">トップページ</button>
		</form>
	</div>
</header>

<body>
	<hr>
	<div align="center">
		<h1 class="Title">授業内容更新</h1>
	</div>
	<p></p>
	<form action="LessonUpdate" method="post">
		<div align="center">
			<nav class="nav flex-column">
				<div align="center">

					<table border="3" width="500">
						<thead>
							<tr align="center">
								<th bgcolor="yellow" width="45%" height="70">授業</th>
								<th bgcolor="yellow" width="65%">詳細</th>
							</tr>
						</thead>

						<tbody>
							<tr>
								<td height="45">授業名</td>
								<td><input style="border: none" type="text" name="LessonN"
									value="${lesson.name}"></td>
							</tr>
							<tr>
								<td height="50">教授名</td>
								<td><input style="border: none" type="text"
									name="professor" value="${lesson.professor}"></td>
							</tr>
							<tr>
								<td height="50">開講曜日・時間</td>
								<td><select name="season">
										<option></option>
										<option>前期</option>
										<option>後期</option>
								</select> <select name="day">
										<option></option>
										<option>月曜</option>
										<option>火曜</option>
										<option>水曜</option>
										<option>木曜</option>
										<option>金曜</option>
										<option>土曜</option>
								</select> <select name="time">
										<option></option>
										<option>1限</option>
										<option>2限</option>
										<option>3限</option>
										<option>4限</option>
										<option>5限</option>
										<option>6限</option>
								</select></td>
							</tr>
							<tr>
								<td height="50">単位数</td>
								<td><select name="credit">
										<option></option>
										<option>2</option>
										<option>1</option>
								</select></td>
							</tr>
							<tr>
								<td height="50">開講場所</td>
								<td><input style="border: none" type="text" name="place"
									value="${lesson.place}"></td>
							</tr>
							<tr>
								<td height="50">内容</td>
								<td><input style="border: none" type="text" name="content"
									value="${lesson.content}"></td>
							<tr>
								<td height="50">試験詳細</td>
								<td><input style="border: none" type="text" name="test"
									value="${lesson.test}"></td>
							</tr>
						</tbody>
					</table>
				</div>
				<p></p>
			</nav>
		</div>

		<div align="center">
			<input type="hidden" name="LeID" value="${lesson.id}"> <input
				type="hidden" name="UniID" value="${university.id }"> <input
				type="hidden" name="StuID" value="${student.id}">
			<button type="submit" class="btn-Update">更新する</button>
			<hr>
		</div>
	</form>

</body>
<footer>
	<div align="right">
        <a href="#" class="btn-flat-logo">
            <i class="fa fa-chevron-right" onclick="history.back()"> 戻る </i>
        </a>
    </div>
</footer>
</html>
