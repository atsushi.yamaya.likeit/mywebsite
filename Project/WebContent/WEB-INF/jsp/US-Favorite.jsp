<%@	page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>US-SearchResultF</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/btn.css">
<link rel="stylesheet" href="css/footer.css">
<link rel="stylesheet" href="css/font.css">
</head>
<header>
	${university.name} ${student.name}

	<div align="right">
		<font size="3"> <a href="Logout" class="Logout">ログアウト</a>
		</font>
		<form action="USTimeTable" method="post">
			<input type="hidden" name="StuID" value="${student.id}"> <input
				type="hidden" name="UniID" value="${university.id }">
			<button type="submit"  class="btn-Update">トップページ</button>
		</form>
	</div>
</header>

<body>
	<hr>
	<div align="center">
		<h1 class="Title">お気に入り友達リスト</h1>
		<table border="3" width="500">
			<thead>
				<tr align="center">
					<th bgcolor="yellow" width="45%" height="70">生徒（ニックネーム）</th>
					<th bgcolor="yellow">履修</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="friend" items="${favoList}">
					<tr>
						<td height="50">${friend.freName}</td>
						<td><a
							href="USFriendsLesson?FreID=${friend.freID}&UniID=${university.id}&StuID=${student.id}">履修を見る</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<p></p>
	</div>
	<hr>
</body>

<footer>
	<div align="right">
        <a href="#" class="btn-flat-logo">
            <i class="fa fa-chevron-right" onclick="history.back()"> 戻る </i>
        </a>
    </div>
</footer>
</html>
