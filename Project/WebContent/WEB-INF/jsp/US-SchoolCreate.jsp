<%@	page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>US-SchoolCreate</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/btn.css">
<link rel="stylesheet" href="css/footer.css">
<link rel="stylesheet" href="css/font.css">
</head>
<header>


	<div align="right">
		<font size="3"> <a href="Logout" class="Logout">ログアウト</a>
		</font>
	</div>
</header>

<body>
	<form action="USSchoolCreate" method="post">
		<hr>
		<div align="center">
			<h1 class="Title">学校詳細</h1>

			<c:if test="${errMsg != null}">
				<font color="red">
					<div class="alert alert-danger" role="alert" align="center">
						${errMsg}</div>
				</font>
			</c:if>

			<div align="center">
				登録する学校名 <input type="text" id="name" name="name" placeholder="学校名">
			</div>

			<p></p>

			<div align="center">
				<button type="submit" class="btn-GroupCreate">登録する</button>
				<hr>
			</div>
		</div>
	</form>
</body>
<footer>
	<div align="right">
		<a href="#" class="btn-flat-logo"> <i class="fa fa-chevron-right"
			onclick="history.back()"> 戻る </i>
		</a>
	</div>
</footer>
</html>

