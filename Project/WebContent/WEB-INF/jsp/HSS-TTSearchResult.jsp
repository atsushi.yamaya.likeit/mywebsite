<%@	page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>HSS-TTSearchResult</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/btn.css">
<link rel="stylesheet" href="css/footer.css">
<link rel="stylesheet" href="css/font.css">
</head>

<header>
	<nav class="red darken-8">
		<div class="nav-wrapper container">${university.name}高校生用ページ</div>
	</nav>

	<div align="right">
		<font size="3"> <a href="Logout" class="Logout">ログアウト</a>
		</font>
	</div>
</header>

<body>
	<p></p>
	<div align="center">
		<h1 class="Title">${student.name}の時間割</h1>

		<hr>

		<div align="center">
			<div>2020年度 前期</div>
			<div class="table-responsive">
				<table width="90%" border="3" cellpadding="10" cellspacing="0">
					<thead align="center">
						<th bgcolor="#00ffff" width="5%"></th>
						<th bgcolor="#00ffff" width="19%">月曜日</th>
						<th bgcolor="#00ffff" width="19%">火曜日</th>
						<th bgcolor="#00ffff" width="19%">水曜日</th>
						<th bgcolor="#00ffff" width="19%">木曜日</th>
						<th bgcolor="#00ffff" width="19%">金曜日</th>

					</thead>
					<tbody align="center">

						<tr>
							<td bgcolor="#00ffff">1</td>
							<td><a
								href="LessonInfo?LeName=${StuTT.mon1}&UniID=${university.id}"
								class="text-dark">${StuTT.mon1}</a></td>
							<td><a
								href="LessonInfo?LeName=${StuTT.tue1}&UniID=${university.id}"
								class="text-dark">${StuTT.tue1}</a></td>
							<td><a
								href="LessonInfo?LeName=${StuTT.wed1}&UniID=${university.id}"
								class="text-dark">${StuTT.wed1}</a></td>
							<td><a
								href="LessonInfo?LeName=${StuTT.thu1}&UniID=${university.id}"
								class="text-dark">${StuTT.thu1}</a></td>
							<td><a
								href="LessonInfo?LeName=${StuTT.fri1}&UniID=${university.id}"
								class="text-dark">${StuTT.fri1}</a></td>
						</tr>
						<tr>
							<td bgcolor="#00ffff">2</td>
							<td><a
								href="LessonInfo?LeName=${StuTT.mon2}&UniID=${university.id}"
								class="text-dark">${StuTT.mon2}</a></td>
							<td><a
								href="LessonInfo?LeName=${StuTT.tue2}&UniID=${university.id}"
								class="text-dark">${StuTT.tue2}</a></td>
							<td><a
								href="LessonInfo?LeName=${StuTT.wed2}&UniID=${university.id}"
								class="text-dark">${StuTT.wed2}</a></td>
							<td><a
								href="LessonInfo?LeName=${StuTT.thu2}&UniID=${university.id}"
								class="text-dark">${StuTT.thu2}</a></td>
							<td><a
								href="LessonInfo?LeName=${StuTT.fri2}&UniID=${university.id}"
								class="text-dark">${StuTT.fri2}</a></td>

						</tr>
						<tr>
							<td bgcolor="#00ffff">3</td>
							<td><a
								href="LessonInfo?LeName=${StuTT.mon3}&UniID=${university.id}"
								class="text-dark">${StuTT.mon3}</a></td>
							<td><a
								href="LessonInfo?LeName=${StuTT.tue3}&UniID=${university.id}"
								class="text-dark">${StuTT.tue3}</a></td>
							<td><a
								href="LessonInfo?LeName=${StuTT.wed3}&UniID=${university.id}"
								class="text-dark">${StuTT.wed3}</a></td>
							<td><a
								href="LessonInfo?LeName=${StuTT.thu3}&UniID=${university.id}"
								class="text-dark">${StuTT.thu3}</a></td>
							<td><a
								href="LessonInfo?LeName=${StuTT.fri3}&UniID=${university.id}"
								class="text-dark">${StuTT.fri3}</a></td>

						</tr>
						<tr>
							<td bgcolor="#00ffff">4</td>
							<td><a
								href="LessonInfo?LeName=${StuTT.mon4}&UniID=${university.id}"
								class="text-dark">${StuTT.mon4}</a></td>
							<td><a
								href="LessonInfo?LeName=${StuTT.tue4}&UniID=${university.id}"
								class="text-dark">${StuTT.tue4}</a></td>
							<td><a
								href="LessonInfo?LeName=${StuTT.wed4}&UniID=${university.id}"
								class="text-dark">${StuTT.wed4}</a></td>
							<td><a
								href="LessonInfo?LeName=${StuTT.thu4}&UniID=${university.id}"
								class="text-dark">${StuTT.thu4}</a></td>
							<td><a
								href="LessonInfo?LeName=${StuTT.fri4}&UniID=${university.id}"
								class="text-dark">${StuTT.fri4}</a></td>

						</tr>
						<tr>
							<td bgcolor="#00ffff">5</td>
							<td><a
								href="LessonInfo?LeName=${StuTT.mon5}&UniID=${university.id}"
								class="text-dark">${StuTT.mon5}</a></td>
							<td><a
								href="LessonInfo?LeName=${StuTT.tue5}&UniID=${university.id}"
								class="text-dark">${StuTT.tue5}</a></td>
							<td><a
								href="LessonInfo?LeName=${StuTT.wed5}&UniID=${university.id}"
								class="text-dark">${StuTT.wed5}</a></td>
							<td><a
								href="LessonInfo?LeName=${StuTT.thu5}&UniID=${university.id}"
								class="text-dark">${StuTT.thu5}</a></td>
							<td><a
								href="LessonInfo?LeName=${StuTT.fri5}&UniID=${university.id}"
								class="text-dark">${StuTT.fri5}</a></td>

						</tr>
					</tbody>
				</table>
			</div>

			<hr>


			<div>後期</div>
			<div class="table-responsive">
				<table width="90%" border="3" cellpadding="10" cellspacing="0">
					<thead align="center">
						<th bgcolor="#00ffff" width="5%"></th>
						<th bgcolor="#00ffff" width="19%">月曜日</th>
						<th bgcolor="#00ffff" width="19%">火曜日</th>
						<th bgcolor="#00ffff" width="19%">水曜日</th>
						<th bgcolor="#00ffff" width="19%">木曜日</th>
						<th bgcolor="#00ffff" width="19%">金曜日</th>

					</thead>
					<tbody align="center">

						<tr>
							<td bgcolor="#00ffff">1</td>
							<td><a
								href="LessonInfo?LeName=${StuTT.mon1k}&UniID=${university.id}"
								class="text-dark">${StuTT.mon1k}</a></td>
							<td><a
								href="LessonInfo?LeName=${StuTT.tue1k}&UniID=${university.id}"
								class="text-dark">${StuTT.tue1k}</a></td>
							<td><a
								href="LessonInfo?LeName=${StuTT.wed1k}&UniID=${university.id}"
								class="text-dark">${StuTT.wed1k}</a></td>
							<td><a
								href="LessonInfo?LeName=${StuTT.thu1k}&UniID=${university.id}"
								class="text-dark">${StuTT.thu1k}</a></td>
							<td><a
								href="LessonInfo?LeName=${StuTT.fri1k}&UniID=${university.id}"
								class="text-dark">${StuTT.fri1k}</a></td>
						</tr>
						<tr>
							<td bgcolor="#00ffff">2</td>
							<td><a
								href="LessonInfo?LeName=${StuTT.mon2k}&UniID=${university.id}"
								class="text-dark">${StuTT.mon2k}</a></td>
							<td><a
								href="LessonInfo?LeName=${StuTT.tue2k}&UniID=${university.id}"
								class="text-dark">${StuTT.tue2k}</a></td>
							<td><a
								href="LessonInfo?LeName=${StuTT.wed2k}&UniID=${university.id}"
								class="text-dark">${StuTT.wed2k}</a></td>
							<td><a
								href="LessonInfo?LeName=${StuTT.thu2k}&UniID=${university.id}"
								class="text-dark">${StuTT.thu2k}</a></td>
							<td><a
								href="LessonInfo?LeName=${StuTT.fri2k}&UniID=${university.id}"
								class="text-dark">${StuTT.fri2k}</a></td>

						</tr>
						<tr>
							<td bgcolor="#00ffff">3</td>
							<td><a
								href="LessonInfo?LeName=${StuTT.mon3k}&UniID=${university.id}"
								class="text-dark">${StuTT.mon3k}</a></td>
							<td><a
								href="LessonInfo?LeName=${StuTT.tue3k}&UniID=${university.id}"
								class="text-dark">${StuTT.tue3k}</a></td>
							<td><a
								href="LessonInfo?LeName=${StuTT.wed3k}&UniID=${university.id}"
								class="text-dark">${StuTT.wed3k}</a></td>
							<td><a
								href="LessonInfo?LeName=${StuTT.thu3k}&UniID=${university.id}"
								class="text-dark">${StuTT.thu3k}</a></td>
							<td><a
								href="LessonInfo?LeName=${StuTT.fri3k}&UniID=${university.id}"
								class="text-dark">${StuTT.fri3k}</a></td>

						</tr>
						<tr>
							<td bgcolor="#00ffff">4</td>
							<td><a
								href="LessonInfo?LeName=${StuTT.mon4k}&UniID=${university.id}"
								class="text-dark">${StuTT.mon4k}</a></td>
							<td><a
								href="LessonInfo?LeName=${StuTT.tue4k}&UniID=${university.id}"
								class="text-dark">${StuTT.tue4k}</a></td>
							<td><a
								href="LessonInfo?LeName=${StuTT.wed4k}&UniID=${university.id}"
								class="text-dark">${StuTT.wed4k}</a></td>
							<td><a
								href="LessonInfo?LeName=${StuTT.thu4k}&UniID=${university.id}"
								class="text-dark">${StuTT.thu4k}</a></td>
							<td><a
								href="LessonInfo?LeName=${StuTT.fri4k}&UniID=${university.id}"
								class="text-dark">${StuTT.fri4k}</a></td>

						</tr>
						<tr>
							<td bgcolor="#00ffff">5</td>
							<td><a
								href="LessonInfo?LeName=${StuTT.mon5k}&UniID=${university.id}"
								class="text-dark">${StuTT.mon5k}</a></td>
							<td><a
								href="LessonInfo?LeName=${StuTT.tue5k}&UniID=${university.id}"
								class="text-dark">${StuTT.tue5k}</a></td>
							<td><a
								href="LessonInfo?LeName=${StuTT.wed5k}&UniID=${university.id}"
								class="text-dark">${StuTT.wed5k}</a></td>
							<td><a
								href="LessonInfo?LeName=${StuTT.thu5k}&UniID=${university.id}"
								class="text-dark">${StuTT.thu5k}</a></td>
							<td><a
								href="LessonInfo?LeName=${StuTT.fri5k}&UniID=${university.id}"
								class="text-dark">${StuTT.fri5k}</a></td>

						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<p></p>
	</div>
	<!-- ↑　時間割表↑　-->
</body>
<footer>
	<div align="right">
		<a href="#" class="btn-flat-logo"> <i class="fa fa-chevron-right"
			onclick="history.back()"> 戻る </i>
		</a>
	</div>
</footer>
</html>
