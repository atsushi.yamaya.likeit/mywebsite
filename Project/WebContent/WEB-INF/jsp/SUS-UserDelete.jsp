<%@	page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>SUS-UserDelete</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="header.css">
    <link rel="stylesheet" href="btn.css">
    <link rel="stylesheet" href="footer.css">
    <link rel="stylesheet" href="font.css">
</head>
<header>
    管理者

    <div align="right">
        <font size="3">
            <a href="#" class="Logout">ログアウト</a>
        </font>
    </div>

</header>

<body>
    <hr>
    <div align="center">
        <h1 class="Title">削除</h1>

        ○○さん
    </div>
<p></p>
    <div align="center">
        このユーザの情報をすべて削除しますか？
    </div>
    <p>
        <div align="center">
            <a href="#" class="btn-square">はい</a>
            <a href="#" class="btn-square">いいえ</a>
        </div>
        <p></p>
        <hr>
</body>
<footer>
    <div align="right">
        <a href="#" class="btn-flat-logo">
            <i class="fa fa-chevron-right" onclick="history.back()"> 戻る </i>
        </a>
    </div>
</footer></html>
