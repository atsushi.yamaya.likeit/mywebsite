<%@	page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>US-TTSet</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/btn.css">
<link rel="stylesheet" href="css/footer.css">
<link rel="stylesheet" href="css/font.css">
</head>
<header>
	${university.name} ${student.name}

	<div align="right">
		<font size="3"> <a href="Logout" class="Logout">ログアウト</a>
		</font>
		<form action="USTimeTable" method="post">
			<input type="hidden" name="StuID" value="${student.id}"> <input
				type="hidden" name="UniID" value="${university.id }">
			<button type="submit" class="btn-Update">トップページ</button>
		</form>
	</div>
</header>

<body>
	<hr>
	<div align="center">
		<h1 class="Title">履修登録・変更</h1>

		<p></p>
		<div align="center">
			<a> </a>

			<form action="LessonCreate">
				<input type="hidden" name="UniID" value="${university.id}">
				<input type="hidden" name="StuID" value="${student.id}">
				<button type="submit" class="btn-Update">新しく授業を登録する</button>
			</form>
		</div>
		<form action="USTTSet" method="post">
			<div align="center">
				<p></p>
				<div>前期</div>

				<div class="table-responsive">
					<table width="80%" border="3" cellpadding="10" cellspacing="0">
						<thead>
							<th bgcolor="#00ffff" width="30"></th>
							<th bgcolor="#00ffff">月曜日</th>
							<th bgcolor="#00ffff">火曜日</th>
							<th bgcolor="#00ffff">水曜日</th>
							<th bgcolor="#00ffff">木曜日</th>
							<th bgcolor="#00ffff">金曜日</th>

						</thead>
						<tbody>

							<tr>
								<td bgcolor="#00ffff">1</td>
								<td width="150"><select name="mon1"><option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>
								<td width="150"><select name="tue1">
										<option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>
								<td width="150"><select name="wed1">
										<option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>
								<td width="150"><select name="thu1">
										<option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>
								<td width="150"><select name="fri1">
										<option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>

							</tr>
							<tr>
								<td bgcolor="#00ffff">2</td>
								<td width="150"><select name="mon2"><option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>
								<td width="150"><select name="tue2">
										<option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>
								<td width="150"><select name="wed2">
										<option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>
								<td width="150"><select name="thu2">
										<option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>
								<td width="150"><select name="fri2">
										<option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>

							</tr>
							<tr>
								<td bgcolor="#00ffff">3</td>
								<td width="150"><select name="mon3"><option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>
								<td width="150"><select name="tue3">
										<option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>
								<td width="150"><select name="wed3">
										<option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>
								<td width="150"><select name="thu3">
										<option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>
								<td width="150"><select name="fri3">
										<option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>

							</tr>
							<tr>
								<td bgcolor="#00ffff">4</td>
								<td width="150"><select name="mon4"><option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>
								<td width="150"><select name="tue4">
										<option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>
								<td width="150"><select name="wed4">
										<option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>
								<td width="150"><select name="thu4">
										<option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>
								<td width="150"><select name="fri4">
										<option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>

							</tr>
							<tr>
								<td bgcolor="#00ffff">5</td>
								<td width="150"><select name="mon5">
										<option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>
								<td width="150"><select name="tue5">
										<option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>
								<td width="150"><select name="wed5">
										<option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>
								<td width="150"><select name="thu5">
										<option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>
								<td width="150"><select name="fri5">
										<option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>
						</tbody>
					</table>
				</div>
				<hr>
				<div>後期</div>
				<div class="table-responsive">
					<table width="80%" border="3" cellpadding="10" cellspacing="0">
						<thead>
							<th bgcolor="#00ffff" width="30"></th>
							<th bgcolor="#00ffff">月曜日</th>
							<th bgcolor="#00ffff">火曜日</th>
							<th bgcolor="#00ffff">水曜日</th>
							<th bgcolor="#00ffff">木曜日</th>
							<th bgcolor="#00ffff">金曜日</th>
						</thead>
						<tbody>

							<tr>
								<td bgcolor="#00ffff">1</td>
								<td width="150"><select name="mon1k">
										<option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>
								<td width="150"><select name="tue1k">
										<option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>
								<td width="150"><select name="wed1k">
										<option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>
								<td width="150"><select name="thu1k">
										<option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>
								<td width="150"><select name="fri1k">
										<option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>

							</tr>
							<tr>
								<td bgcolor="#00ffff">2</td>
								<td width="150"><select name="mon2k"><option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>
								<td width="150"><select name="tue2k">
										<option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>
								<td width="150"><select name="wed2k">
										<option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>
								<td width="150"><select name="thu2k">
										<option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>
								<td width="150"><select name="fri2k">
										<option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>

							</tr>
							<tr>
								<td bgcolor="#00ffff">3</td>
								<td width="150"><select name="mon3k"><option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>
								<td width="150"><select name="tue3k">
										<option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>
								<td width="150"><select name="wed3k">
										<option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>
								<td width="150"><select name="thu3k">
										<option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>
								<td width="150"><select name="fri3k">
										<option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>

							</tr>
							<tr>
								<td bgcolor="#00ffff">4</td>
								<td width="150"><select name="mon4k"><option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>
								<td width="150"><select name="tue4k">
										<option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>
								<td width="150"><select name="wed4k">
										<option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>
								<td width="150"><select name="thu4k">
										<option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>
								<td width="150"><select name="fri4k">
										<option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>

							</tr>
							<tr>
								<td bgcolor="#00ffff">5</td>
								<td width="150"><select name="mon5k"><option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>
								<td width="150"><select name="tue5k">
										<option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>
								<td width="150"><select name="wed5k">
										<option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>
								<td width="150"><select name="thu5k">
										<option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>
								<td width="150"><select name="fri5k">
										<option></option>
										<c:forEach var="lesson" items="${LessonList}">
											<option>${lesson.name}</option>
										</c:forEach>
								</select></td>
						</tbody>
					</table>
				</div>
			</div>
			<p></p>
			<input type="hidden" name="StuID" value="${student.id}"> <input
				type="hidden" name="UniID" value="${university.id }">
			<button type="submit" class="btn-Create">この内容で登録する</button>
		</form>
	</div>
	<p></p>
	<hr>

</body>
<footer>
<div align="right">
        <a href="#" class="btn-flat-logo">
            <i class="fa fa-chevron-right" onclick="history.back()"> 戻る </i>
        </a>
    </div>
</footer>
</html>
