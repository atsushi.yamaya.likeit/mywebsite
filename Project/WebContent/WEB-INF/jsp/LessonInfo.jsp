<%@	page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>LessonInfo</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/btn.css">
<link rel="stylesheet" href="css/footer.css">
<link rel="stylesheet" href="css/font.css">
</head>
<header>
	<nav class="red darken-8">
		<div class="nav-wrapper container">${university.name}
			${student.name}</div>
	</nav>

	<div align="right">
		<font size="3"> <a href="Logout" class="Logout">ログアウト</a>
		</font>
		<form action="USTimeTable" method="post">
			<input type="hidden" name="StuID" value="${student.id}"> <input
				type="hidden" name="UniID" value="${university.id }">
			<button type="submit" class="btn-Update">トップページ</button>
		</form>

	</div>
</header>

<body>
	<hr>
	<div align="center">
		<h1 class="Title">授業詳細</h1>
	</div>

	<c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>

	<div align="center">
		<table border="3" width="500">
			<tr align="center">
				<th bgcolor="yellow" width="45%" height="70">授業</th>
				<th bgcolor="yellow">詳細</th>
			</tr>
			<tr>
				<td height="50">授業名</td>
				<td>${lesson.name}</td>
			</tr>
			<tr>
				<td height="50">教授名</td>
				<td>${lesson.professor}</td>
			</tr>
			<tr>
				<td height="50">開講曜日・時間</td>
				<td>${lesson.season}${lesson.day}${lesson.time}</td>
			</tr>
			<tr>
				<td height="50">単位数</td>
				<td>${lesson.credit}</td>
			</tr>
			<tr>
				<td height="50">開講場所</td>
				<td>${lesson.place}</td>
			</tr>
			<tr>
				<td height="50">内容</td>
				<td>${lesson.content}</td>
			<tr>
				<td height="50">試験詳細</td>
				<td>${lesson.test}</td>
			</tr>

		</table>
		<p></p>
	</div>

	<p align="center">
		<c:if test="${student.id != null }">
			<a
				href="LessonUpdate?LeID=${lesson.id}&UniID=${university.id}&StuID=${student.id}"
				type="submit" class="btn-Update">この授業の詳細を変更する </a>
		</c:if>
		<a
			href="LessonReview?LeID=${lesson.id}&UniID=${university.id}&StuID=${student.id}"
			type="submit" class="btn-Update">この授業のレビューを見る・書く </a>
	<hr>

	<div align="center">
		<c:if test="${student.id != null }">
			<form action="LessonDelete">
				<button type="submit" class="btn-Update">この授業を削除する</button>
				<input type="hidden" name="StuID" value="${student.id}"> <input
					type="hidden" name="UniID" value="${university.id }"> <input
					type="hidden" name="LeID" value="${lesson.id}">
			</form>
		</c:if>
	</div>
	<p></p>
</body>

<footer>
	<div align="right">
        <a href="#" class="btn-flat-logo">
            <i class="fa fa-chevron-right" onclick="history.back()"> 戻る </i>
        </a>
    </div>
</footer>
</html>
