<%@	page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>US-TTSearchResultL</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/btn.css">
<link rel="stylesheet" href="css/footer.css">
<link rel="stylesheet" href="css/font.css">
</head>
<header>
	${university.name} ${student.name }


	<div align="right">
		<font size="3"> <a href="Logout" class="Logout">ログアウト</a>
		</font>
		<form action="USTimeTable" method="post">
			<input type="hidden" name="StuID" value="${student.id}"> <input
				type="hidden" name="UniID" value="${university.id }">
			<button type="submit" class="btn-Update">トップページ</button>
		</form>
	</div>
</header>

<body>
	<hr>
	<div align="center">
		<h1 class="Title">授業を選択してください</h1>
	</div>


	<div align="center">
		前期 〇曜日 〇限

		<table border="3" width="90%">
			<tr align="center">
				<th bgcolor="yellow" width="30%" height="70">授業</th>
				<th bgcolor="yellow" width="20%">教授名</th>
				<th bgcolor="yellow" width="20%">内容</th>
				<th bgcolor="yellow" width="10%">履修</th>
			</tr>
			<tr>
				<td height="50" align="center"><a
					href="LessonInfo?=LeID=${lesson.id}&UniID=${university.id}&StuID=${student.id}">ゼミ</a></td>
				<td>教授名</td>
				<td></td>
				<td><button>履修する</button></td>
			</tr>
		</table>
		<p></p>
	</div>

	<hr>
</body>

<footer>
	<div align="right">
        <a href="#" class="btn-flat-logo">
            <i class="fa fa-chevron-right" onclick="history.back()"> 戻る </i>
        </a>
    </div>
</footer>
</html>
