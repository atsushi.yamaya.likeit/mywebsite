<%@	page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>US-SearchResultF</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/btn.css">
<link rel="stylesheet" href="css/footer.css">
<link rel="stylesheet" href="css/font.css">
</head>
<header>
	${university.name} ${student.name}

	<div align="right">
		<font size="3"> <a href="Logout" class="Logout">ログアウト</a>
		</font>
		<form action="USTimeTable" method="post">
			<input type="hidden" name="StuID" value="${student.id}"> <input
				type="hidden" name="UniID" value="${university.id }">
			<button type="submit"  class="btn-Update">トップページ</button>
		</form>
	</div>
</header>

<body>
	<hr>
	<div align="center">
		<h1 class="Title">検索結果</h1>
	</div>

	<div align="center">
		<table border="3" width="500">
			<thead>
				<tr align="center">
					<th bgcolor="yellow" width="50%" height="60">友達</th>
					<th bgcolor="yellow">お気に入り登録</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="Friend" items="${SearchListF }">
					<tr align="center">
						<td height="50"><a
							href="USFriendsLesson?LeID=${lesson.id}&UniID=${university.id}&StuID=${student.id}&FreID=${Friend.id}"
							type="submit">${Friend.name}</a></td>
						<td>
							<div align="center">
								<a
									href="USFavorite?LeID=${lesson.id}&UniID=${university.id}&StuID=${student.id}&FreID=${Friend.id}"
									type="submit">友達リストに追加</a>
							</div>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<p></p>
	</div>

	<hr>
</body>

<footer>
<div align="right">
        <a href="#" class="btn-flat-logo">
            <i class="fa fa-chevron-right" onclick="history.back()"> 戻る </i>
        </a>
    </div>
</footer>
</html>
