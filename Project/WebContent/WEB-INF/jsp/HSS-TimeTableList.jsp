<%@	page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>HSS-TimeTableList</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/btn.css">
<link rel="stylesheet" href="css/footer.css">
<link rel="stylesheet" href="css/font.css">
</head>
<header>
	<nav class="red darken-8">
		<div class="nav-wrapper container">${university.name} 高校生用ページ</div>
	</nav>

	<div align="right">
		<font size="3"> <a href="Logout" class="Logout">ログアウト</a>
		</font>
	</div>
</header>

<body>
	<p></p>
	<div align="center">
		<h2 class="Title">実際の生徒の履修</h2>
	</div>

	<hr>
	<div align="center">
		<table border="3" width="500">
			<thead>
				<tr align="center">
					<th bgcolor="yellow" width="45%" height="70">生徒（ニックネーム）</th>
					<th bgcolor="yellow">詳細</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="students" items="${StudentsList}">
					<tr>
						<td height="50">${students.name}</td>
						<td><a
							href="HSSTTSearchResult?StuID=${students.id}&UniID=${university.id}">履修を見る</a></td>
					</tr>

				</c:forEach>
			</tbody>
		</table>
		<p></p>
	</div>

	<hr>
</body>
<footer>
	<div align="right">
        <a href="#" class="btn-flat-logo">
            <i class="fa fa-chevron-right" onclick="history.back()"> 戻る </i>
        </a>
    </div>
</footer>
</html>
