<%@	page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>TopPage</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/btn.css">
<link rel="stylesheet" href="css/footer.css">
<link rel="stylesheet" href="css/font.css">
</head>



<header position="fixed">

	<nav class="red darken-8">
		<div class="nav-wrapper container">トップページ</div>
	</nav>

</header>

<body>
	<div align="center">
		<h1 class="TimeTableShare">大学履修管理</h1>
	</div>
	<div align="right">
		<a href="USSchoolCreate" class="Title">学校を新しく作る</a>
	</div>

	<!--↓大学ログイン↓-->
	<div align="center">
		<form action="USLogin" method="get">
			<h3 class="Title">大学ログイン</h3>
			<input type="text" placeholder="大学名" id="name" name="name">
			<p></p>
			<button type="submit" class="btn-SchoolCreate">ログインへ</button>
		</form>
	</div>

	<hr>
	<!--↑大学ログイン↑-->

	<!--↓大学一覧↓-->
	<div align="center">
		<h2 class="Title">大学名参照</h2>
	</div>

	<div align="center">
		<table border="3" width="500">

			<thead>
				<tr align="center">
					<th bgcolor="yellow" width="45%" height="70">大学名</th>
					<th bgcolor="yellow">⇩高校生用履修見学⇩</th>
				</tr>
			</thead>

			<tbody>
				<c:forEach var="university" items="${universityList }">
					<tr>
						<td height="50">${university.name}</td>
						<td><a href="HSSLogin?UniID=${university.id}">履修を見る</a></td>
					</tr>

				</c:forEach>
			</tbody>
		</table>
		<p></p>
	</div>
	<!--↑大学一覧↑-->

	<hr>
</body>
<footer>

</footer></html>
