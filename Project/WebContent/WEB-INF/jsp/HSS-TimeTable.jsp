<%@	page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>HSS-TimeTable</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/btn.css">
<link rel="stylesheet" href="css/footer.css">
<link rel="stylesheet" href="css/font.css">
</head>

<header>
	<nav class="red darken-8">
		<div class="nav-wrapper container">${university.name} 高校生用ページ</div>
	</nav>

	<div align="right">
		<font size="3"> <a href="Logout" class="Logout">ログアウト</a>
		</font>
	</div>

</header>

<body>

	<hr>

	<div align="right">
		<a href="HSSTimeTableList?UniID=${university.id}"
			class="WatchTimeTable"> 実際の生徒の履修を見る</a>
	</div>


	<!--↓授業検索↓-->

	<div align="center">

		<div align="center">
			<h2 class="Title">授業検索</h2>

			<c:if test="${errMsg != null}">
				<div class="alert alert-danger" role="alert">${errMsg}</div>
			</c:if>

			<form action="HSSLogin" method="post">
				気になる授業名 <span class="font-weight-bolder"> <input type="text"
					placeholder="授業名" id="lessonN" name="lessonN">（部分一致） <input
					type="hidden" name="UniID" value="${university.id}">
				</span>
				<p></p>
				<button type="submit" class="btn-Search">調べる</button>
			</form>
		</div>
	</div>


	<hr>
	<!-- ↑授業検索↑ -->

	<!--↓授業一覧↓-->

	<div align="center" class="Title">
		<font size="5">授業一覧</font>
	</div>
	<div align="center">
		<table border="2" width="600">
			<thead>
				<tr align="center">
					<th bgcolor="yellow" height="50" width="30%">授業名</th>
					<th bgcolor="yellow" width="120%">内容</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="lesson" items="${SearchListL}">
					<tr>
						<td height="50"><a
							href="LessonInfo?LeID=${lesson.id}&UniID=${university.id}">${lesson.name}</a></td>
						<td>${lesson.content}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	<p></p>
	<!--↑授業一覧↑-->

	<hr>

</body>
<footer>
	<div align="right">
        <a href="#" class="btn-flat-logo">
            <i class="fa fa-chevron-right" onclick="history.back()"> 戻る </i>
        </a>
    </div>
</footer>
</html>
