<%@	page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>SUS-School'sLesson</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="header.css">
    <link rel="stylesheet" href="btn.css">
    <link rel="stylesheet" href="footer.css">
    <link rel="stylesheet" href="font.css">
</head>
<header>
    管理者
    <div align="right">
        <font size="3">
            <a href="#" class="Logout">ログアウト</a>
        </font>
    </div>
</header>

<body>


    <hr>
    <form>
        <div align="center">
            <h1 class="Title">〇〇大学の履修</h1>

            <div align="center">
                <a href="#" class="btn-square">授業新規登録</a>
            </div>

            <p></p>
            <div align="center">
                <font size="6">
                    すべての授業
                </font>
                <p></p>

                <!--↓時間割表↓-->

                <div align="center">
                    <div>2020年度　前期</div>
                    <div class="table-responsive">
                        <table width="80%" border="3" cellpadding="10" cellspacing="0">
                            <thead>
                                <th bgcolor="#00ffff" width="30"></th>
                                <th bgcolor="#00ffff">月曜日</th>
                                <th bgcolor="#00ffff">火曜日</th>
                                <th bgcolor="#00ffff">水曜日</th>
                                <th bgcolor="#00ffff">木曜日</th>
                                <th bgcolor="#00ffff">金曜日</th>
                                <th bgcolor="#00ffff">土曜日</th>
                            </thead>
                            <tbody>

                                <tr>
                                    <td bgcolor="#00ffff">1</td>
                                    <td><a href="#" class="text-dark">仕事の社会学</a></td>
                                    <td><a href="#" class="text-dark">政治経済学</a></td>
                                    <td><a href="#" class="text-dark">現代政治経済学</a></td>
                                    <td><a href="#" class="text-dark">マーケティング</a></td>
                                    <td><a href="#" class="text-dark">映画論</a></td>
                                    <td><a href="#" class="text-dark">ゼミ</a></td>
                                </tr>
                                <tr>
                                    <td bgcolor="#00ffff">2</td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                </tr>
                                <tr>
                                    <td bgcolor="#00ffff">3</td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                </tr>
                                <tr>
                                    <td bgcolor="#00ffff">4</td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                </tr>
                                <tr>
                                    <td bgcolor="#00ffff">5</td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                </tr>
                                <tr>
                                    <td bgcolor="#00ffff">6</td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                    <p></p>

                    <p>2020年度　後期</p>
                    <div class="table-responsive">
                        <table width="80%" border="3" cellpadding="10" cellspacing="0">
                            <thead>
                                <th bgcolor="#00ffff" width="30"></th>
                                <th bgcolor="#00ffff">月曜日</th>
                                <th bgcolor="#00ffff">火曜日</th>
                                <th bgcolor="#00ffff">水曜日</th>
                                <th bgcolor="#00ffff">木曜日</th>
                                <th bgcolor="#00ffff">金曜日</th>
                                <th bgcolor="#00ffff">土曜日</th>
                            </thead>
                            <tbody>

                                <tr>
                                    <td bgcolor="#00ffff">1</td>
                                    <td><a href="#" class="text-dark">仕事の社会学</a></td>
                                    <td><a href="#" class="text-dark">政治経済学</a></td>
                                    <td><a href="#" class="text-dark">現代政治経済学</a></td>
                                    <td><a href="#" class="text-dark">マーケティング</a></td>
                                    <td><a href="#" class="text-dark">映画論</a></td>
                                    <td><a href="#" class="text-dark">ゼミ</a></td>
                                </tr>
                                <tr>
                                    <td bgcolor="#00ffff">2</td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                </tr>
                                <tr>
                                    <td bgcolor="#00ffff">3</td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                </tr>
                                <tr>
                                    <td bgcolor="#00ffff">4</td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                </tr>
                                <tr>
                                    <td bgcolor="#00ffff">5</td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                </tr>
                                <tr>
                                    <td bgcolor="#00ffff">6</td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                    <td width="150"></td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
                <p></p>
                <p></p>
            </div>
        </div>
    </form>
    <hr>

</body>
<footer>
    <div align="right">
       <div class="btn-flat-logo">
            <button class="fa fa-chevron-right" onclick="history.back()"> 戻る </button>
          </div>
    </div>
</footer></html>
