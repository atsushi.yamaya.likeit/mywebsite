<%@	page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>SUS-UserUpdate</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="header.css">
    <link rel="stylesheet" href="btn.css">
    <link rel="stylesheet" href="footer.css">
    <link rel="stylesheet" href="font.css">
</head>
<header position="fixed">

    <nav class="red darken-8">
        <div class="nav-wrapper container">
            管理者
        </div>
    </nav>

    <div align="right">
        <font size="3">
            <a href="#" class="Logout">ログアウト</a>
        </font>
    </div>

</header>

<body>
    <p></p>
    <div align="center">
        <h1 class="Title">〇〇の詳細変更</h1>
        <div>
            <label>ニックネーム</label>
            <input type="text" placeholder="ニックネーム">
        </div>
        <p></p>
        <div align="center">
            ログインID
            <input type="text" placeholder="ログインID">
        </div>
        <p></p>
        <div align="center">
            パスワード
            <input type="text" placeholder="パスワード">
        </div>
        <p></p>
        <div align="center">
            パスワード（確認）
            <input type="text" placeholder="パスワード（確認）">
        </div>
    </div>
    <p></p>
    <div align="center">
        <button type="submit" class="btn-Update">変更する</button>
        <hr>
    </div>
    <div align="center">

        <h1 class="Title">〇〇の履修変更</h1>
        <div>前期</div>
        <div class="table-responsive">
            <table width="80%" border="3" cellpadding="10" cellspacing="0">
                <thead>
                    <th bgcolor="#00ffff" width="30"></th>
                    <th bgcolor="#00ffff">月曜日</th>
                    <th bgcolor="#00ffff">火曜日</th>
                    <th bgcolor="#00ffff">水曜日</th>
                    <th bgcolor="#00ffff">木曜日</th>
                    <th bgcolor="#00ffff">金曜日</th>
                    <th bgcolor="#00ffff">土曜日</th>
                </thead>
                <tbody>

                    <tr>
                        <td bgcolor="#00ffff">1</td>
                        <td width="150"><a href="US-TTSearchResultL.html">⇩</a><a href="LessonInfo.html" class="lesson"></a></td>
                        <td width="150"><a href="US-TTSearchResultL.html">⇩</a><a href="LessonInfo.html" class="lesson"></a></td>
                        <td width="150"><a href="US-TTSearchResultL.html">⇩</a><a href="LessonInfo.html" class="lesson">ゼミ</a></td>
                        <td width="150"><a href="US-TTSearchResultL.html">⇩</a><a href="LessonInfo.html" class="lesson">国際経済</a></td>
                        <td width="150"><a href="US-TTSearchResultL.html">⇩</a><a href="LessonInfo.html" class="lesson"></a></td>
                        <td width="150"><a href="US-TTSearchResultL.html">⇩</a><a href="LessonInfo.html" class="lesson">国際経済</a></td>
                    </tr>
                    <tr>
                        <td bgcolor="#00ffff">1</td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                    </tr>
                    <tr>
                        <td bgcolor="#00ffff">1</td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson">マーケ</a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson">国際経済</a></td>
                    </tr>
                    <tr>
                        <td bgcolor="#00ffff">1</td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson">ゼミ</a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                    </tr>
                    <tr>
                        <td bgcolor="#00ffff">1</td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                    </tr>
                    <tr>
                        <td bgcolor="#00ffff">1</td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson">ゼミ</a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson">映画論</a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                    </tr>

                </tbody>
            </table>
        </div>
        <hr>
        <div>後期</div>
        <div class="table-responsive">
            <table width="80%" border="3" cellpadding="10" cellspacing="0">
                <thead>
                    <th bgcolor="#00ffff" width="30"></th>
                    <th bgcolor="#00ffff">月曜日</th>
                    <th bgcolor="#00ffff">火曜日</th>
                    <th bgcolor="#00ffff">水曜日</th>
                    <th bgcolor="#00ffff">木曜日</th>
                    <th bgcolor="#00ffff">金曜日</th>
                    <th bgcolor="#00ffff">土曜日</th>
                </thead>
                <tbody>

                    <tr>
                        <td bgcolor="#00ffff">1</td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson">ゼミ</a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson">国際経済</a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson">国際経済</a></td>
                    </tr>
                    <tr>
                        <td bgcolor="#00ffff">1</td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                    </tr>
                    <tr>
                        <td bgcolor="#00ffff">1</td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson">マーケ</a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson">国際経済</a></td>
                    </tr>
                    <tr>
                        <td bgcolor="#00ffff">1</td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson">ゼミ</a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                    </tr>
                    <tr>
                        <td bgcolor="#00ffff">1</td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                    </tr>
                    <tr>
                        <td bgcolor="#00ffff">1</td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson">ゼミ</a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson">映画論</a></td>
                        <td width="150"><a href="#">⇩</a><a href="#" class="lesson"></a></td>
                    </tr>

                </tbody>
            </table>
        </div>
    </div>

    <div align="center">
        <button type="submit" class="btn-Update">変更する</button>
        <hr>
    </div>

</body>
<footer>
    <div align="right">
        <a href="#" class="btn-flat-logo">
            <i class="fa fa-chevron-right" onclick="history.back()"> 戻る </i>
        </a>
    </div>
</footer></html>
