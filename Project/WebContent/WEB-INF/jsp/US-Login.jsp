<%@	page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>US-Login</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/btn.css">
<link rel="stylesheet" href="css/footer.css">
<link rel="stylesheet" href="css/font.css">
</head>

<header>
	${university.name}

	<div align="right" font-size="30">
		<a href="USLoginInfoSet?UniID=${university.id}" class="text-danger">
			新規登録</a>
	</div>
	<div align="right">
		<font size="3"> <a href="Logout" class="Logout">ログアウト</a>
		</font>
	</div>
</header>


<body>
	<div align="center">
		<h1 id="TimeTableShare">時間割表シェア</h1>
	</div>
	<div align="center">
		<h2 class="Title">${university.name}</h2>
	</div>
	<c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>

	<!--↓記入欄↓-->
	<form action="USTimeTable">
		<input type="hidden" name="UniID" value="${university.id}">

		<div align="center">
			ログインID <input type="text" placeholder="ログインID" id="login_id"
				name="login_id">
		</div>
		<p></p>
		<div align="center">
			パスワード <input type="text" placeholder="パスワード" id="password"
				name="password">
		</div>
		<p></p>
		<div align="center">
			<button type="submit" class="btn btn-primary">ログイン</button>
		</div>
	</form>
	<!--↑記入欄↑-->
	<hr>
</body>

<footer>
	<div align="right">
        <a href="#" class="btn-flat-logo">
            <i class="fa fa-chevron-right" onclick="history.back()"> 戻る </i>
        </a>
    </div>
</footer>
</html>
