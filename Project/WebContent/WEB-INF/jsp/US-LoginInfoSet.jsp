<%@	page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>US-LoginInfoSet</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/btn.css">
<link rel="stylesheet" href="css/footer.css">
<link rel="stylesheet" href="css/font.css">
</head>

<header>
	${university.name}
	<div align="right">
		<font size="3"> <a href="Logout" class="Logout">ログアウト</a>
		</font>
	</div>
</header>

<body>
	<div align="center">
		<h1 id="TimeTableShare">時間割表シェア</h1>
	</div>
	<c:if test="${errMsg != null}">
		<font color="red">
			<div class="alert alert-danger" role="alert" align="center">
				${errMsg}</div>
		</font>
	</c:if>
	<div align="center">
		<h2 class="Title">${university.name}</h2>
	</div>

	<!--↓記入欄↓-->
	<form action="USLoginInfoSet" method="post">
		<input type="hidden" name="UniID" value="${university.id}">

		<div align="center">
			ニックネーム <input type="text" placeholder="ニックネーム" id="name" name="name">
		</div>
		<p></p>
		<div align="center">
			ログインID <input type="text" placeholder="ログインID" id="login_id"
				name="login_id">
		</div>
		<p></p>
		<div align="center">
			パスワード <input type="text" placeholder="パスワード" id="password"
				name="password">
		</div>
		<p></p>
		<div align="center">
			パスワード（確認） <input type="text" placeholder="パスワード（確認)" id="passwordC"
				name="passwordC">
		</div>

		<p></p>
		<div align="center">
			<button type="submit" class="btn btn-primary">アカウントを作成する</button>
		</div>
	</form>
	<!--↑記入欄↑-->
	<hr>
</body>

<footer>
	<div align="right">
        <a href="#" class="btn-flat-logo">
            <i class="fa fa-chevron-right" onclick="history.back()"> 戻る </i>
        </a>
    </div>
</footer>
</html>
