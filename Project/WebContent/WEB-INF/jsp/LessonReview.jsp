<%@	page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>LessonReview</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/btn.css">
<link rel="stylesheet" href="css/footer.css">
<link rel="stylesheet" href="css/font.css">
</head>
<header>

	${university.name} ${student.name}

	<div align="right">
		<font size="3"> <a href="Logout" class="Logout">ログアウト</a>
		</font>

		   <form action="USTimeTable" method="post">
		<input type="hidden" name="StuID" value="${student.id}">
   		 <input type="hidden" name="UniID" value="${university.id }">
   		 <button type="submit"  class="btn-Update">トップページ</button>
  </form>

	</div>
</header>

<body>
	<hr>
	<div align="center">
		<h1 class="Title">授業レビュー</h1>
	</div>

	<div align="center">
		<h2>授業名　${lesson.name}</h2>
	</div>

	<div align="center" class="UnderLine">
		平均評価 <span class="ReviewPoint">${ReviewAve}</span>点
	</div>
	<c:if test="${student.id != null}">
		<div align="center">
			<div>レビューを書く</div>
			<form action="ReviewSet" method="get">

				自分の評価 <select name="review">
					<option>10</option>
					<option>9</option>
					<option>8</option>
					<option>7</option>
					<option>6</option>
					<option>5</option>
					<option>4</option>
					<option>3</option>
					<option>2</option>
					<option>1</option>
				</select>
				<p></p>
				<textarea rows="3" cols="40 " name="impress"></textarea>
				<input type="hidden" name="LeID" value="${lesson.id}"> <input
					type="hidden" name="UniID" value="${university.id}"> <input
					type="hidden" name="StuID" value="${student.id }">
				<p></p>
				<button>投稿する</button>
			</form>
		</div>
	</c:if>
	<hr>

	<div align="center">
		<table border="3" width="500">
			<thead>
				<tr align="center">
					<th bgcolor="yellow" width="30%" height="70">ニックネーム</th>
					<th bgcolor="yellow" width="15%" height="70">授業評価</th>
					<th bgcolor="yellow">感想</th>
				</tr>
			</thead>
			<c:forEach var="review" items="${ReviewList}">
				<tbody>
					<tr align="center">
						<td height="50">${review.name}</td>
						<td>${review.review}</td>
						<td>${review.impress}</td>
					</tr>
				</tbody>
			</c:forEach>
			<p></p>
		</table>
	</div>
	<p></p>

	<hr>
</body>

<footer>
		<div align="right">
        <a href="#" class="btn-flat-logo">
            <i class="fa fa-chevron-right" onclick="history.back()"> 戻る </i>
        </a>
    </div>
</footer>
</html>
