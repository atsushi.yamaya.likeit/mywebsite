<%@	page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>US-InfoDelete</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/btn.css">
<link rel="stylesheet" href="css/footer.css">
<link rel="stylesheet" href="css/font.css">
</head>
<header>
	${university.name} ${student.name}

	<div align="right">
		<font size="3"> <a href="Logout" class="Logout">ログアウト</a>
		</font>
		<form action="USTimeTable" method="post">
			<input type="hidden" name="StuID" value="${student.id}"> <input
				type="hidden" name="UniID" value="${university.id }">
			<button type="submit" class="btn-Update">トップページ</button>
		</form>
	</div>

</header>

<body>
	<hr>
	<div align="center">
		<h1 class="Title">削除</h1>
	</div>
	<div align="center">自分(${student.name})の情報をすべて削除しますか？</div>
	<p>
	<div align="center">
		<form action="USInfoDelete" method="post">
			<input type="hidden" name="StuID" value="${student.id}"> <input
				type="hidden" name="UniID" value="${university.id }">
			<button type="submit" class="btn-square">はい</button>
		</form>
		<form action="USTimeTable" method="post">
			<input type="hidden" name="StuID" value="${student.id}"> <input
				type="hidden" name="UniID" value="${university.id }">
			<button type="submit" class="btn-square">いいえ</button>
		</form>
	</div>
	<p></p>
	<hr>
</body>
<footer>
	<div align="right">
        <a href="#" class="btn-flat-logo">
            <i class="fa fa-chevron-right" onclick="history.back()"> 戻る </i>
        </a>
    </div>
</footer>
</html>
