<%@	page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>US-InfoUpdate</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/header.css">
<link rel="stylesheet" href="css/btn.css">
<link rel="stylesheet" href="css/footer.css">
<link rel="stylesheet" href="css/font.css">
</head>
<header position="fixed">

	<nav class="red darken-8">
		<div class="nav-wrapper container">${university.name}
			${student.name}</div>
	</nav>

	<div align="right">
		<font size="3"> <a href="Logout" class="Logout">ログアウト</a>
		</font>
		<form action="USTimeTable" method="post">
			<input type="hidden" name="StuID" value="${student.id}"> <input
				type="hidden" name="UniID" value="${university.id }">
			<button type="submit" class="btn-Update">トップページ</button>
		</form>
	</div>

</header>

<body>
	<p></p>
	<form action="USInfoUpdate" method="post">
		<div align="center">
			<h1 class="Title">自分の詳細変更</h1>

			<c:if test="${errMsg != null}">
				<div class="alert alert-danger" role="alert">${errMsg}</div>
			</c:if>

			<div align="center">ログインID ${student.loginId}</div>
			<input type="hidden" name="StuID" value="${student.id}"> <input
				type="hidden" name="UniID" value="${university.id }">
			<p></p>
			<div>
				<label>ニックネーム</label> <input type="text" value="${student.name }"
					id="name" name="name">
			</div>
			<p></p>
			<p></p>
			<div align="center">
				パスワード <input type="text" placeholder="パスワード" id="password"
					name="password">
			</div>
			<p></p>
			<div align="center">
				パスワード（確認） <input type="text" placeholder="パスワード（確認)" id="passwordC"
					name="passwordC">
			</div>
		</div>
		<p></p>
		<div align="center">
			<button type="submit" class="btn-Update">変更する</button>
			<hr>
		</div>
	</form>

</body>
<footer>
	<div align="right">
        <a href="#" class="btn-flat-logo">
            <i class="fa fa-chevron-right" onclick="history.back()"> 戻る </i>
        </a>
    </div>
</footer>
</html>
