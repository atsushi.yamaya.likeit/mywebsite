package beans;

public class Review {
	private int id;
	private int leID;
	private int review;
	private String impress;
	private String name;


	public Review(int iD, int leID, int review, String impress) {
		this.id = iD;
		this.leID = leID;
		this.review = review;
		this.impress = impress;



	}

	public Review(int iD2, int leID2, int review2, String impress2, String name) {
		this.id = iD2;
		this.leID = leID2;
		this.review = review2;
		this.impress = impress2;
		this.name = name;



	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getLeID() {
		return leID;
	}

	public void setLeID(int leID) {
		this.leID = leID;
	}

	public int getReview() {
		return review;
	}

	public void setReview(int review) {
		this.review = review;
	}

	public String getImpress() {
		return impress;
	}

	public void setImpress(String impress) {
		this.impress = impress;
	}



}
