package beans;

public class studentDateBeans {

	public int id;
	private String name;
	private String loginId;
	private String password;
	private int uniId;
	private int stuIDC;


	public studentDateBeans(int id, String name) {
		this.id = id;
		this.name = name;

	}

	public studentDateBeans(int id2, String loginId, String password, String name2, int uniId) {


		this.id = id2;
		this.loginId = loginId;
		this.password = password;
		this.name = name2;
		this.uniId = uniId;


	}



	public studentDateBeans(int stuIDC) {

		this.stuIDC = stuIDC;

	}

	public int getStuIDC() {
		return stuIDC;
	}

	public void setStuIDC(int stuIDC) {
		this.stuIDC = stuIDC;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getUniId() {
		return uniId;
	}

	public void setUniId(int uniId) {
		this.uniId = uniId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}