package beans;

public class Lesson {


	private int id;
	private int university_id;
	private String name;
	private String professor;
	private String season;
	private String day;
	private String time;
	private String credit;
	private String place;
	private String content;
	private String test;

	public Lesson(int id, String name,String professor, String season, String day, String time, String credit, String place,
			String content, String test) {

		this.id = id;
		this.name= name;
		this.professor = professor;
		this.season = season;
		this.day = day;
		this.time = time;
		this.credit = credit;
		this.place = place;
		this.content = content;
		this.test = test;

	}

	public Lesson(int id2, int uniID, String name2, String professor2, String season2, String day2, String time2,
			String credit2, String place2, String content2, String test2) {

		this.id = id2;
		this.university_id = uniID;
		this.name= name2;
		this.professor = professor2;
		this.season = season2;
		this.day = day2;
		this.time = time2;
		this.credit = credit2;
		this.place = place2;
		this.content = content2;
		this.test = test2;


	}

	public int getUniversity_id() {
		return university_id;
	}

	public void setUniversity_id(int university_id) {
		this.university_id = university_id;
	}

	public String getProfessor() {
		return professor;
	}

	public void setProfessor(String professor) {
		this.professor = professor;
	}

	public String getSeason() {
		return season;
	}

	public void setSeason(String season) {
		this.season = season;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getCredit() {
		return credit;
	}

	public void setCredit(String credit) {
		this.credit = credit;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getTest() {
		return test;
	}

	public void setTest(String test) {
		this.test = test;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}

