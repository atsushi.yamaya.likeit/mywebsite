package beans;

public class University{
	
	private int id;
	private String name;
	private String professor;
	private String season;
	private String day;
	private String time;
	private String credit;
	private String place;
	private String content;
	private String test;
	

	public University(int id, String name){
		this.id = id;
		this.name = name;
		

	}

	public University(int id, String Name,String professor, String season, String day, String time, String credit, String place,
			String content, String test) {
		
		this.id = id;
		this.name = Name;
		this.professor = professor;
		this.season = season;
		this.day = day;
		this.time = time;
		this.credit = credit;
		this.place = place;
		this.content = content;
		this.test = test;
		
	}

	public String getProfessor() {
		return professor;
	}

	public void setProfessor(String professor) {
		this.professor = professor;
	}

	public String getSeason() {
		return season;
	}

	public void setSeason(String season) {
		this.season = season;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getCredit() {
		return credit;
	}

	public void setCredit(String credit) {
		this.credit = credit;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getTest() {
		return test;
	}

	public void setTest(String test) {
		this.test = test;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
