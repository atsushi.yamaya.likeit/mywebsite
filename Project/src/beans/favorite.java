package beans;

public class favorite {

	private int id;
	private int myID;
	private int freID;
	private String freName;


	public favorite(int id, int myID, int freID) {

		this.id = id;
		this.myID = myID;
		this.freID = freID;
	}
	public favorite(int id2, int myID2, int freID2, String freName) {

		this.id = id2;
		this.myID = myID2;
		this.freID = freID2;
		this.freName = freName;

	}
	public String getFreName() {
		return freName;
	}
	public void setFreName(String freName) {
		this.freName = freName;
	}
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public int getMyID() {
		return myID;
	}


	public void setMyID(int myID) {
		this.myID = myID;
	}


	public int getFreID() {
		return freID;
	}


	public void setFreID(int freID) {
		this.freID = freID;
	}


}
