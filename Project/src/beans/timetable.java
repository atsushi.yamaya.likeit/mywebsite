package beans;

public class timetable {

	private int stuID;
	private String mon1;
	private String mon2;
	private String mon3;
	private String mon4;
	private String mon5;
	private String tue1;
	private String tue2;
	private String tue3;
	private String tue4;
	private String tue5;
	private String wed1;
	private String wed2;
	private String wed3;
	private String wed4;
	private String wed5;
	private String thu1;
	private String thu2;
	private String thu3;
	private String thu4;
	private String thu5;
	private String fri1;
	private String fri2;
	private String fri3;
	private String fri4;
	private String fri5;
	private String mon1k;
	private String mon2k;
	private String mon3k;
	private String mon4k;
	private String mon5k;
	private String tue1k;
	private String tue2k;
	private String tue3k;
	private String tue4k;
	private String tue5k;
	private String wed1k;
	private String wed2k;
	private String wed3k;
	private String wed4k;
	private String wed5k;
	private String thu1k;
	private String thu2k;
	private String thu3k;
	private String thu4k;
	private String thu5k;
	private String fri1k;
	private String fri2k;
	private String fri3k;
	private String fri4k;
	private String fri5k;

	public timetable(int stuID, String mon1, String mon2, String mon3, String mon4, String mon5, String tue1,
			String tue2, String tue3, String tue4, String tue5, String wed1, String wed2, String wed3, String wed4,
			String wed5, String thu1, String thu2, String thu3, String thu4, String thu5, String fri1, String fri2,
			String fri3, String fri4, String fri5, String mon1k, String mon2k, String mon3k, String mon4k, String mon5k,
			String tue1k, String tue2k, String tue3k, String tue4k, String tue5k, String wed1k, String wed2k,
			String wed3k, String wed4k, String wed5k, String thu1k, String thu2k, String thu3k, String thu4k,
			String thu5k, String fri1k, String fri2k, String fri3k, String fri4k, String fri5k) {

		this.mon1 = mon1;
		this.mon2 = mon2;
		this.mon3 = mon3;
		this.mon4 = mon4;
		this.mon5 = mon5;
		this.tue1 = tue1;
		this.tue2 = tue2;
		this.tue3 = tue3;
		this.tue4 = tue4;
		this.tue5 = tue5;
		this.wed1 = wed1;
		this.wed2 = wed2;
		this.wed3 = wed3;
		this.wed4 = wed4;
		this.wed5 = wed5;
		this.thu1 = thu1;
		this.thu2 = thu2;
		this.thu3 = thu3;
		this.thu4 = thu4;
		this.thu5 = thu5;
		this.fri1 = fri1;
		this.fri2 = fri2;
		this.fri3 = fri3;
		this.fri4 = fri4;
		this.fri5 = fri5;
		this.mon1k = mon1k;
		this.mon2k = mon2k;
		this.mon3k = mon3k;
		this.mon4k = mon4k;
		this.mon5k = mon5k;
		this.tue1k = tue1k;
		this.tue2k = tue2k;
		this.tue3k = tue3k;
		this.tue4k = tue4k;
		this.tue5k = tue5k;
		this.wed1k = wed1k;
		this.wed2k = wed2k;
		this.wed3k = wed3k;
		this.wed4k = wed4k;
		this.wed5k = wed5k;
		this.thu1k = thu1k;
		this.thu2k = thu2k;
		this.thu3k = thu3k;
		this.thu4k = thu4k;
		this.thu5k = thu5k;
		this.fri1k = fri1k;
		this.fri2k = fri2k;
		this.fri3k = fri3k;
		this.fri4k = fri4k;
		this.fri5k = fri5k;

	}

	public int getStuID() {
		return stuID;
	}

	public void setStuID(int stuID) {
		this.stuID = stuID;
	}

	public String getMon1() {
		return mon1;
	}

	public void setMon1(String mon1) {
		this.mon1 = mon1;
	}

	public String getMon2() {
		return mon2;
	}

	public void setMon2(String mon2) {
		this.mon2 = mon2;
	}

	public String getMon3() {
		return mon3;
	}

	public void setMon3(String mon3) {
		this.mon3 = mon3;
	}

	public String getMon4() {
		return mon4;
	}

	public void setMon4(String mon4) {
		this.mon4 = mon4;
	}

	public String getMon5() {
		return mon5;
	}

	public void setMon5(String mon5) {
		this.mon5 = mon5;
	}

	public String getTue1() {
		return tue1;
	}

	public void setTue1(String tue1) {
		this.tue1 = tue1;
	}

	public String getTue2() {
		return tue2;
	}

	public void setTue2(String tue2) {
		this.tue2 = tue2;
	}

	public String getTue3() {
		return tue3;
	}

	public void setTue3(String tue3) {
		this.tue3 = tue3;
	}

	public String getTue4() {
		return tue4;
	}

	public void setTue4(String tue4) {
		this.tue4 = tue4;
	}

	public String getTue5() {
		return tue5;
	}

	public void setTue5(String tue5) {
		this.tue5 = tue5;
	}

	public String getWed1() {
		return wed1;
	}

	public void setWed1(String wed1) {
		this.wed1 = wed1;
	}

	public String getWed2() {
		return wed2;
	}

	public void setWed2(String wed2) {
		this.wed2 = wed2;
	}

	public String getWed3() {
		return wed3;
	}

	public void setWed3(String wed3) {
		this.wed3 = wed3;
	}

	public String getWed4() {
		return wed4;
	}

	public void setWed4(String wed4) {
		this.wed4 = wed4;
	}

	public String getWed5() {
		return wed5;
	}

	public void setWed5(String wed5) {
		this.wed5 = wed5;
	}

	public String getThu1() {
		return thu1;
	}

	public void setThu1(String thu1) {
		this.thu1 = thu1;
	}

	public String getThu2() {
		return thu2;
	}

	public void setThu2(String thu2) {
		this.thu2 = thu2;
	}

	public String getThu3() {
		return thu3;
	}

	public void setThu3(String thu3) {
		this.thu3 = thu3;
	}

	public String getThu4() {
		return thu4;
	}

	public void setThu4(String thu4) {
		this.thu4 = thu4;
	}

	public String getThu5() {
		return thu5;
	}

	public void setThu5(String thu5) {
		this.thu5 = thu5;
	}

	public String getFri1() {
		return fri1;
	}

	public void setFri1(String fri1) {
		this.fri1 = fri1;
	}

	public String getFri2() {
		return fri2;
	}

	public void setFri2(String fri2) {
		this.fri2 = fri2;
	}

	public String getFri3() {
		return fri3;
	}

	public void setFri3(String fri3) {
		this.fri3 = fri3;
	}

	public String getFri4() {
		return fri4;
	}

	public void setFri4(String fri4) {
		this.fri4 = fri4;
	}

	public String getFri5() {
		return fri5;
	}

	public void setFri5(String fri5) {
		this.fri5 = fri5;
	}

	public String getMon1k() {
		return mon1k;
	}

	public void setMon1k(String mon1k) {
		this.mon1k = mon1k;
	}

	public String getMon2k() {
		return mon2k;
	}

	public void setMon2k(String mon2k) {
		this.mon2k = mon2k;
	}

	public String getMon3k() {
		return mon3k;
	}

	public void setMon3k(String mon3k) {
		this.mon3k = mon3k;
	}

	public String getMon4k() {
		return mon4k;
	}

	public void setMon4k(String mon4k) {
		this.mon4k = mon4k;
	}

	public String getMon5k() {
		return mon5k;
	}

	public void setMon5k(String mon5k) {
		this.mon5k = mon5k;
	}

	public String getTue1k() {
		return tue1k;
	}

	public void setTue1k(String tue1k) {
		this.tue1k = tue1k;
	}

	public String getTue2k() {
		return tue2k;
	}

	public void setTue2k(String tue2k) {
		this.tue2k = tue2k;
	}

	public String getTue3k() {
		return tue3k;
	}

	public void setTue3k(String tue3k) {
		this.tue3k = tue3k;
	}

	public String getTue4k() {
		return tue4k;
	}

	public void setTue4k(String tue4k) {
		this.tue4k = tue4k;
	}

	public String getTue5k() {
		return tue5k;
	}

	public void setTue5k(String tue5k) {
		this.tue5k = tue5k;
	}

	public String getWed1k() {
		return wed1k;
	}

	public void setWed1k(String wed1k) {
		this.wed1k = wed1k;
	}

	public String getWed2k() {
		return wed2k;
	}

	public void setWed2k(String wed2k) {
		this.wed2k = wed2k;
	}

	public String getWed3k() {
		return wed3k;
	}

	public void setWed3k(String wed3k) {
		this.wed3k = wed3k;
	}

	public String getWed4k() {
		return wed4k;
	}

	public void setWed4k(String wed4k) {
		this.wed4k = wed4k;
	}

	public String getWed5k() {
		return wed5k;
	}

	public void setWed5k(String wed5k) {
		this.wed5k = wed5k;
	}

	public String getThu1k() {
		return thu1k;
	}

	public void setThu1k(String thu1k) {
		this.thu1k = thu1k;
	}

	public String getThu2k() {
		return thu2k;
	}

	public void setThu2k(String thu2k) {
		this.thu2k = thu2k;
	}

	public String getThu3k() {
		return thu3k;
	}

	public void setThu3k(String thu3k) {
		this.thu3k = thu3k;
	}

	public String getThu4k() {
		return thu4k;
	}

	public void setThu4k(String thu4k) {
		this.thu4k = thu4k;
	}

	public String getThu5k() {
		return thu5k;
	}

	public void setThu5k(String thu5k) {
		this.thu5k = thu5k;
	}

	public String getFri1k() {
		return fri1k;
	}

	public void setFri1k(String fri1k) {
		this.fri1k = fri1k;
	}

	public String getFri2k() {
		return fri2k;
	}

	public void setFri2k(String fri2k) {
		this.fri2k = fri2k;
	}

	public String getFri3k() {
		return fri3k;
	}

	public void setFri3k(String fri3k) {
		this.fri3k = fri3k;
	}

	public String getFri4k() {
		return fri4k;
	}

	public void setFri4k(String fri4k) {
		this.fri4k = fri4k;
	}

	public String getFri5k() {
		return fri5k;
	}

	public void setFri5k(String fri5k) {
		this.fri5k = fri5k;
	}



}
