package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.favorite;

public class favoriteDAO {

	public static void Set(String stuID,String FreID) throws SQLException {

		Connection conn = null;
		try {

			conn = DBManager.getConnection();
				String sql = "INSERT INTO favorite(my_id,friend_id) VALUES (?,?)";

				PreparedStatement pStmt = conn.prepareStatement(sql);

				pStmt.setString(1, stuID);
				pStmt.setString(2, FreID);

				int rs = pStmt.executeUpdate();


		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static List<favorite> Search(String stuID) {

		Connection conn = null;
		List<favorite> favoList = new ArrayList<favorite>();

		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM favorite"
					+ " JOIN student"
					+ " ON favorite.friend_id = student.id"
					+ " WHERE my_id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, stuID);
			ResultSet rs = pStmt.executeQuery();

			while(rs.next()) {
				int ID = rs.getInt("id");
				int MyID = rs.getInt("my_id");
				int FreID = rs.getInt("friend_id");
				String FreName = rs.getString("name");


				favorite friend = new favorite(ID,MyID,FreID,FreName);
				favoList.add(friend);

			}

			}catch (SQLException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
		return favoList;
		}
	}

