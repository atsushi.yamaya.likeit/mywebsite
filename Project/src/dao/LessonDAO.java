package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.Lesson;
import beans.timetable;

public class LessonDAO {

	public static List<Lesson> LessonInfo(String id) throws SQLException {

		Connection conn = null;
		List<Lesson> LessonList = new ArrayList<Lesson>();

		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM lesson"

					+ " JOIN university"
					+ " ON lesson.university_id = university.id"

					+ " WHERE lesson.university_id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1,id);
			ResultSet rs = pStmt.executeQuery();

			while(rs.next()) {
				int Id = rs.getInt("id");
				String Name = rs.getString("name");
				String Professor = rs.getString("professor");
				String Season = rs.getString("season");
				String Day = rs.getString("day");
				String time = rs.getString("time");
				String Credit = rs.getString("credit");
				String place = rs.getString("place");
				String Content = rs.getString("content");
				String Test = rs.getString("test");

				Lesson lesson = new Lesson(Id,Name,Professor,Season,Day,time,Credit,place,Content,Test);

				LessonList.add(lesson);
			}
			return LessonList;


		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
		}
	}
	}

	public static Lesson Info(String id) {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();
		String sql = "SELECT * FROM lesson WHERE lesson.id = ?";

		PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);

		ResultSet rs = pStmt.executeQuery();

		if (!rs.next()) {
			return null;
		}

		int Id = rs.getInt("id");
		int UniID = rs.getInt("university_id");
		String Name = rs.getString("name");
		String Professor = rs.getString("professor");
		String Season = rs.getString("season");
		String Day = rs.getString("day");
		String time = rs.getString("time");
		String Credit = rs.getString("credit");
		String place = rs.getString("place");
		String Content = rs.getString("content");
		String Test = rs.getString("test");

		Lesson lesson = new Lesson(Id,UniID,Name,Professor,Season,Day,time,Credit,place,Content,Test);

		return lesson;

	} catch (SQLException e) {
		e.printStackTrace();
		return null;
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
		}
	}
	}
	}


	public static List<Lesson> SearchLesson(String uniID, String lessonN) {

		Connection conn = null;
		List<Lesson> SearchListL = new ArrayList<Lesson>();

		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM lesson WHERE university_id = ? and name LIKE ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, uniID);
			pStmt.setString(2, "%" +lessonN+ "%");
			ResultSet rs = pStmt.executeQuery();

			while(rs.next()) {
				int id = rs.getInt("id");
				int UniID = rs.getInt("university_id");
				String name = rs.getString("name");
				String Professor = rs.getString("professor");
				String Season = rs.getString("season");
				String Day = rs.getString("day");
				String Time = rs.getString("time");
				String Credit = rs.getString("credit");
				String Place = rs.getString("place");
				String Content = rs.getString("content");
				String Test = rs.getString("test");

				Lesson lesson = new Lesson(id,UniID,name,Professor,Season,Day,Time,Credit,Place,Content,Test);
				SearchListL.add(lesson);

			}



		}catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
		return SearchListL;
	}

	public static void LessonUpdate(String LeID,String lessonN, String professor, String season, String day,
			String time, String credit, String place, String content, String test) {

		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "UPDATE lesson SET name=?,professor=?,season=?,day=?,time=?,credit=?,place=?,content=?,test=? WHERE id=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, lessonN);
			pStmt.setString(2, professor);
			pStmt.setString(3, season);
			pStmt.setString(4, day);
			pStmt.setString(5, time);
			pStmt.setString(6,credit);
			pStmt.setString(7,place);
			pStmt.setString(8, content);
			pStmt.setString(9, test);
			pStmt.setString(10, LeID);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void LessonCreate(String uniID, String lessonN, String professor, String season, String day,
			String time, String credit, String place, String content, String test) {

		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "INSERT INTO lesson (university_id,name,professor,season,day,time,credit,place,content,test) VALUE (?,?,?,?,?,?,?,?,?,?)";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, uniID);
			pStmt.setString(2, lessonN);
			pStmt.setString(3, professor);
			pStmt.setString(4, season);
			pStmt.setString(5, day);
			pStmt.setString(6, time);
			pStmt.setString(7,credit);
			pStmt.setString(8,place);
			pStmt.setString(9, content);
			pStmt.setString(10, test);

			int rs = pStmt.executeUpdate();

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public static void LessonDelete(String leID) throws SQLException {

		 Connection conn = null;
		 try {
			 conn = DBManager.getConnection();
			 String sql = "DELETE FROM lesson WHERE id = ?";

			 PreparedStatement pStmt = conn.prepareStatement(sql);
			 pStmt.setString(1, leID);

			 int rs = pStmt.executeUpdate();

		 }finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}


	}

	public static List<Lesson> LessonList(String UniID) throws SQLException {

		Connection conn = null;
		List<Lesson> LessonList = new ArrayList<Lesson>();

		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM university "
					+ " JOIN lesson"
					+ " ON university.id = lesson.university_id"
					+ " WHERE university.id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, UniID);
			ResultSet rs = pStmt.executeQuery();

			while(rs.next()) {
				int id = rs.getInt("id");
				int uniID = rs.getInt("university_id");
				String Lname = rs.getString("lesson.name");
				String Professor = rs.getString("professor");
				String Season = rs.getString("season");
				String Day = rs.getString("day");
				String Time = rs.getString("time");
				String Credit = rs.getString("credit");
				String Place = rs.getString("place");
				String Content = rs.getString("content");
				String Test = rs.getString("test");

				Lesson lesson = new Lesson(id,uniID,Lname,Professor,Season,Day,Time,Credit,Place,Content,Test);
				LessonList.add(lesson);
			}
		}finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return LessonList;
	}

	public static Lesson NameSearch(String UniID,String LeName) {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();
		String sql = "SELECT * FROM lesson WHERE university_id=? and name=?";

		PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, UniID);
			pStmt.setString(2, LeName);

		ResultSet rs = pStmt.executeQuery();

		if (!rs.next()) {
			return null;
		}

		int Id = rs.getInt("id");
		int uniID = rs.getInt("university_id");
		String Name = rs.getString("name");
		String Professor = rs.getString("professor");
		String Season = rs.getString("season");
		String Day = rs.getString("day");
		String time = rs.getString("time");
		String Credit = rs.getString("credit");
		String place = rs.getString("place");
		String Content = rs.getString("content");
		String Test = rs.getString("test");

		Lesson lesson = new Lesson(Id,uniID,Name,Professor,Season,Day,time,Credit,place,Content,Test);

		return lesson;

	} catch (SQLException e) {
		e.printStackTrace();
		return null;
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
		}
	}
	}
	}

	public static timetable LoginSearch(int id) {

		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM tt WHERE student_id=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, id);
			ResultSet rs = pStmt.executeQuery();

			if(!rs.next()) {
				return null;
			}

			int StuID = rs.getInt("student_id");
			String mon1 = rs.getString("mon1");
			String mon2 = rs.getString("mon2");
			String mon3 = rs.getString("mon3");
			String mon4 = rs.getString("mon4");
			String mon5 = rs.getString("mon5");
			String tue1 = rs.getString("tue1");
			String tue2 = rs.getString("tue2");
			String tue3 = rs.getString("tue3");
			String tue4 = rs.getString("tue4");
			String tue5 = rs.getString("tue5");
			String wed1 = rs.getString("wed1");
			String wed2 = rs.getString("wed2");
			String wed3 = rs.getString("wed3");
			String wed4 = rs.getString("wed4");
			String wed5 = rs.getString("wed5");
			String thu1 = rs.getString("thu1");
			String thu2 = rs.getString("thu2");
			String thu3 = rs.getString("thu3");
			String thu4 = rs.getString("thu4");
			String thu5 = rs.getString("thu5");
			String fri1 = rs.getString("fri1");
			String fri2 = rs.getString("fri2");
			String fri3 = rs.getString("fri3");
			String fri4 = rs.getString("fri4");
			String fri5 = rs.getString("fri5");
			String mon1k = rs.getString("mon1k");
			String mon2k = rs.getString("mon2k");
			String mon3k = rs.getString("mon3k");
			String mon4k = rs.getString("mon4k");
			String mon5k = rs.getString("mon5k");
			String tue1k = rs.getString("tue1k");
			String tue2k = rs.getString("tue2k");
			String tue3k = rs.getString("tue3k");
			String tue4k = rs.getString("tue4k");
			String tue5k = rs.getString("tue5k");
			String wed1k = rs.getString("wed1k");
			String wed2k = rs.getString("wed2k");
			String wed3k = rs.getString("wed3k");
			String wed4k = rs.getString("wed4k");
			String wed5k = rs.getString("wed5k");
			String thu1k = rs.getString("thu1k");
			String thu2k = rs.getString("thu2k");
			String thu3k = rs.getString("thu3k");
			String thu4k = rs.getString("thu4k");
			String thu5k = rs.getString("thu5k");
			String fri1k = rs.getString("fri1k");
			String fri2k = rs.getString("fri2k");
			String fri3k = rs.getString("fri3k");
			String fri4k = rs.getString("fri4k");
			String fri5k = rs.getString("fri5k");

			timetable StuTT = new timetable(StuID,mon1,mon2,mon3,mon4,mon5,tue1,tue2,tue3,tue4,tue5,wed1,wed2,wed3,wed4,wed5,thu1,thu2,thu3,thu4,thu5,fri1,fri2,fri3,fri4,fri5,mon1k,mon2k,mon3k,mon4k,mon5k,tue1k,tue2k,tue3k,tue4k,tue5k,wed1k,wed2k,wed3k,wed4k,wed5k,thu1k,thu2k,thu3k,thu4k,thu5k,fri1k,fri2k,fri3k,fri4k,fri5k);
			return StuTT;



		}catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

}

