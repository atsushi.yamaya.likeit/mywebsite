package dao;

import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.studentDateBeans;

public class studentDAO {


	public static List<studentDateBeans> Search(String UniID) {

		Connection conn = null;
		List<studentDateBeans> StudentsList = new ArrayList<studentDateBeans>();

		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM student"

					+ " JOIN university"
					+ " ON student.university_id = university.id"

					+ " WHERE student.university_id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, UniID);
			ResultSet rs = pStmt.executeQuery();

			while(rs.next()) {
				int Id = rs.getInt("id");
				String Name = rs.getString("name");

				studentDateBeans students = new studentDateBeans(Id,Name);
				StudentsList.add(students);

			}

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
		}

	}
		return StudentsList;
	}

	public static void Set(String loginId, String password, String name,String UniID) throws NoSuchAlgorithmException {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "INSERT INTO student (login_id,password,name,university_id) VALUES (?,?,?,?)";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1,loginId);
			pStmt.setString(2,password);
			pStmt.setString(3,name);
			pStmt.setString(4,UniID);

			int rs = pStmt.executeUpdate();

		}catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
	}
	}

	public static studentDateBeans LoginSchool(String loginId, String password,String UniId){

		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM student WHERE login_id = ? and password = ? and university_id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2,password);
			pStmt.setString(3, UniId);
			ResultSet rs = pStmt.executeQuery();

			if(!rs.next()) {
				return null;
			}


			int id = rs.getInt("id");
			String name = rs.getString("name");

			studentDateBeans student = new studentDateBeans(id,name);

			return student;

		}catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
	}
	}

	public static studentDateBeans USInfo(String StuID) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM student WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1,StuID);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int Id = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String password = rs.getString("password");
			String name = rs.getString("name");
			int UniId = rs.getInt("university_id");

			studentDateBeans student = new studentDateBeans(Id,loginId,password,name,UniId);
			return student;



		}catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
	}
	}

	public static void InfoUpdate(String StuID, String password,String name) {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();
			String sql = "UPDATE student SET password = ?,name = ? WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, password);
			pStmt.setString(2, name);
			pStmt.setString(3, StuID);

			pStmt.executeUpdate();


		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void USInfoDelete(String StuID) throws SQLException {

		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "DELETE FROM student WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, StuID);

			int rs = pStmt.executeUpdate();

		}finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static List<studentDateBeans> SearchFriend(String UniID,String USName) throws SQLException{

		Connection conn = null;
		List<studentDateBeans> SearchListF = new ArrayList<studentDateBeans>();

		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM student WHERE university_id = ? and name LIKE ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, UniID);
			pStmt.setString(2, "%" +USName+ "%");
			ResultSet rs = pStmt.executeQuery();

			while(rs.next()) {
				int StuID = rs.getInt("id");
				String LoginID = rs.getString("login_id");
				String password = rs.getString("password");
				String Name = rs.getString("name");
				int uniID = rs.getInt("university_id");

				studentDateBeans Friend = new studentDateBeans(StuID,LoginID,password,Name,uniID);

				SearchListF.add(Friend);
			}

		}finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
		}
		}
		return SearchListF;
	}

	public static studentDateBeans Check(String StuID) {

		Connection conn = null;
		try {
			conn = DBManager.getConnection();
 			String sql = "SELECT * FROM tt WHERE student_id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, StuID);
			ResultSet rs = pStmt.executeQuery();

			if(!rs.next()) {
				return null;
			}
			int id = rs.getInt("id");
			int stuIDC = rs.getInt("student_id");
			studentDateBeans stuDateCheck = new studentDateBeans(stuIDC);

			return stuDateCheck;

			}catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
		}
	}

	public static studentDateBeans LoginCheck(String loginId) {

		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM student WHERE login_id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);

			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int Id = rs.getInt("id");
			String LoginId = rs.getString("login_id");

			studentDateBeans Check = new studentDateBeans(Id,LoginId);
			return Check;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
			}
			}
		}
	}
}

