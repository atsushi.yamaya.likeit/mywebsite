package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.Review;

public class reviewDAO {

	public static List<Review> Search(String LeID) {
		List<Review> ReviewList = new ArrayList<Review>();

		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM  review "
					+ " JOIN student"
					+ " ON review.student_id = student.id"
					+ " WHERE review.lesson_id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, LeID);
			ResultSet rs = pStmt.executeQuery();

			while(rs.next()) {
				int ID = rs.getInt("id");
				int leID = rs.getInt("lesson_id");
				int Review = rs.getInt("review");
				String Impress = rs.getString("impress");
				String Name = rs.getString("name");



				Review review = new Review(ID,leID,Review,Impress,Name);

				ReviewList.add(review);

			}


		}catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

		return ReviewList;
	}

	public static void Set(String leID, String review, String impress, String stuID) throws SQLException {

		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "INSERT INTO review (lesson_id,review,impress,student_id) VALUES (?,?,?,?)";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, leID);
			pStmt.setString(2, review);
			pStmt.setString(3, impress);
			pStmt.setString(4, stuID);

			int rs = pStmt.executeUpdate();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public static double ave(String LeID) throws SQLException {

		Connection conn = null;
		PreparedStatement st = null;
		try {
			conn = DBManager.getConnection();
			st = conn.prepareStatement("SELECT AVG(review) as avg FROM review WHERE lesson_id = ?");
			st.setString(1, LeID);
			ResultSet rs =st.executeQuery();

			double ReviewAve = 0.0;
			while(rs.next()) {
				ReviewAve = rs.getDouble("avg");
			}

			return ReviewAve;

		}finally {
			if (conn != null) {
				conn.close();
	}
}
	}
}
