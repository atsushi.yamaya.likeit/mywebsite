package dao;

import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.University;

public class universityDAO {

	public static List<University> finduniList() {

		Connection conn = null;
		List<University> universityList = new ArrayList<University>();

		try {

			conn = DBManager.getConnection();
			String sql = "SELECT * FROM university";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while(rs.next()) {
				int UniID = rs.getInt("id");
				String name = rs.getString("name");

				University university = new University(UniID,name);

				universityList.add(university);
			}


		}catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return universityList;
	}

	public static University Search(String UniID) {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM university WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, UniID);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int uniID = rs.getInt("id");
			String Name = rs.getString("name");

			University university = new University(uniID,Name);

			return university;


		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
	}

	}

	public static void Create(String name) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "INSERT INTO university(name) VALUES (?)";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, name);

			int rs = pStmt.executeUpdate();



		}catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static University Login(String name) throws NoSuchAlgorithmException {

		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM university WHERE name = ? ";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, name);

			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int id = rs.getInt("id");
			String Name = rs.getString("name");
			University university = new University(id, Name);

			return university;


		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
	}
}

	public static University Check(String name) {

		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM university WHERE name = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, name);
			ResultSet rs = pStmt.executeQuery();

			if(!rs.next()) {
				return null;
			}

			int id = rs.getInt("id");
			String Name = rs.getString("name");

			University Check = new University(id,Name);

			return Check;

		}catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

	}


}



