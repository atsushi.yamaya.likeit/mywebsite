package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import base.DBManager;
import beans.timetable;

public class timetableDAO {

	public static void create(String stuID, String mon1, String mon2, String mon3, String mon4, String mon5,
			String tue1, String tue2, String tue3, String tue4, String tue5, String wed1, String wed2, String wed3,
			String wed4, String wed5, String thu1, String thu2, String thu3, String thu4, String thu5, String fri1,
			String fri2, String fri3, String fri4, String fri5, String mon1k, String mon2k, String mon3k, String mon4k,
			String mon5k, String tue1k, String tue2k, String tue3k, String tue4k, String tue5k, String wed1k,
			String wed2k, String wed3k, String wed4k, String wed5k, String thu1k, String thu2k, String thu3k,
			String thu4k, String thu5k, String fri1k, String fri2k, String fri3k, String fri4k, String fri5k) {

		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "INSERT INTO tt (student_id,mon1,mon2,mon3,mon4,mon5,tue1,tue2,tue3,tue4,tue5,wed1,wed2,wed3,wed4,wed5,thu1,thu2,thu3,thu4,thu5,fri1,fri2,fri3,fri4,fri5, mon1k,mon2k,mon3k,mon4k,mon5k,tue1k,tue2k,tue3k,tue4k,tue5k,wed1k,wed2k,wed3k,wed4k,wed5k,thu1k,thu2k,thu3k,thu4k,thu5k,fri1k,fri2k,fri3k,fri4k,fri5k) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

					PreparedStatement pStmt = conn.prepareStatement(sql);

				pStmt.setString(1,stuID);
				pStmt.setString(2, mon1);
				pStmt.setString(3, mon2);
				pStmt.setString(4, mon3);
				pStmt.setString(5, mon4);
				pStmt.setString(6, mon5);
				pStmt.setString(7, tue1);
				pStmt.setString(8, tue2);
				pStmt.setString(9, tue3);
				pStmt.setString(10, tue4);
				pStmt.setString(11, tue5);
				pStmt.setString(12, wed1);
				pStmt.setString(13, wed2);
				pStmt.setString(14, wed3);
				pStmt.setString(15, wed4);
				pStmt.setString(16, wed5);
				pStmt.setString(17, thu1);
				pStmt.setString(18, thu2);
				pStmt.setString(19, thu3);
				pStmt.setString(20, thu4);
				pStmt.setString(21, thu5);
				pStmt.setString(22, fri1);
				pStmt.setString(23, fri2);
				pStmt.setString(24, fri3);
				pStmt.setString(25, fri4);
				pStmt.setString(26, fri5);
				pStmt.setString(27, mon1k);
				pStmt.setString(28, mon2k);
				pStmt.setString(29, mon3k);
				pStmt.setString(30, mon4k);
				pStmt.setString(31, mon5k);
				pStmt.setString(32, tue1k);
				pStmt.setString(33, tue2k);
				pStmt.setString(34, tue3k);
				pStmt.setString(35, tue4k);
				pStmt.setString(36, tue5k);
				pStmt.setString(37, wed1k);
				pStmt.setString(38, wed2k);
				pStmt.setString(39, wed3k);
				pStmt.setString(40, wed4k);
				pStmt.setString(41, wed5k);
				pStmt.setString(42, thu1k);
				pStmt.setString(43, thu2k);
				pStmt.setString(44, thu3k);
				pStmt.setString(45, thu4k);
				pStmt.setString(46, thu5k);
				pStmt.setString(47, fri1k);
				pStmt.setString(48, fri2k);
				pStmt.setString(49, fri3k);
				pStmt.setString(50, fri4k);
				pStmt.setString(51, fri5k);

				int rs = pStmt.executeUpdate();


		}catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}


	}
	}

	public static void update(String stuID, String mon1, String mon2, String mon3, String mon4, String mon5,
			String tue1, String tue2, String tue3, String tue4, String tue5, String wed1, String wed2, String wed3,
			String wed4, String wed5, String thu1, String thu2, String thu3, String thu4, String thu5, String fri1,
			String fri2, String fri3, String fri4, String fri5, String mon1k, String mon2k, String mon3k, String mon4k,
			String mon5k, String tue1k, String tue2k, String tue3k, String tue4k, String tue5k, String wed1k,
			String wed2k, String wed3k, String wed4k, String wed5k, String thu1k, String thu2k, String thu3k,
			String thu4k, String thu5k, String fri1k, String fri2k, String fri3k, String fri4k, String fri5k) {

		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "UPDATE tt SET mon1=?,mon2=?,mon3=?,mon4=?,mon5=?,tue1=?,tue2=?,tue3=?,tue4=?,tue5=?,wed1=?,wed2=?,wed3=?,wed4=?,wed5=?,thu1=?,thu2=?,thu3=?,thu4=?,thu5=?,fri1=?,fri2=?,fri3=?,fri4=?,fri5=?,mon1k=?,mon2k=?,mon3k=?,mon4k=?,mon5k=?,tue1k=?,tue2k=?,tue3k=?,tue4k=?,tue5k=?,wed1k=?,wed2k=?,wed3k=?,wed4k=?,wed5k=?,thu1k=?,thu2k=?,thu3k=?,thu4k=?,thu5k=?,fri1k=?,fri2k=?,fri3k=?,fri4k=?,fri5k=? WHERE student_id =?";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, mon1);
			pStmt.setString(2, mon2);
			pStmt.setString(3, mon3);
			pStmt.setString(4, mon4);
			pStmt.setString(5, mon5);
			pStmt.setString(6, tue1);
			pStmt.setString(7, tue2);
			pStmt.setString(8, tue3);
			pStmt.setString(9, tue4);
			pStmt.setString(10, tue5);
			pStmt.setString(11, wed1);
			pStmt.setString(12, wed2);
			pStmt.setString(13, wed3);
			pStmt.setString(14, wed4);
			pStmt.setString(15, wed5);
			pStmt.setString(16, thu1);
			pStmt.setString(17, thu2);
			pStmt.setString(18, thu3);
			pStmt.setString(19, thu4);
			pStmt.setString(20, thu5);
			pStmt.setString(21, fri1);
			pStmt.setString(22, fri2);
			pStmt.setString(23, fri3);
			pStmt.setString(24, fri4);
			pStmt.setString(25, fri5);
			pStmt.setString(26, mon1k);
			pStmt.setString(27, mon2k);
			pStmt.setString(28, mon3k);
			pStmt.setString(29, mon4k);
			pStmt.setString(30, mon5k);
			pStmt.setString(31, tue1k);
			pStmt.setString(32, tue2k);
			pStmt.setString(33, tue3k);
			pStmt.setString(34, tue4k);
			pStmt.setString(35, tue5k);
			pStmt.setString(36, wed1k);
			pStmt.setString(37, wed2k);
			pStmt.setString(38, wed3k);
			pStmt.setString(39, wed4k);
			pStmt.setString(40, wed5k);
			pStmt.setString(41, thu1k);
			pStmt.setString(42, thu2k);
			pStmt.setString(43, thu3k);
			pStmt.setString(44, thu4k);
			pStmt.setString(45, thu5k);
			pStmt.setString(46, fri1k);
			pStmt.setString(47, fri2k);
			pStmt.setString(48, fri3k);
			pStmt.setString(49, fri4k);
			pStmt.setString(50, fri5k);
			pStmt.setString(51,stuID);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static timetable Search(String stuID) {

		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM tt WHERE student_id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, stuID);
			ResultSet rs = pStmt.executeQuery();

			if(!rs.next()) {
				return null;
			}

			int id = rs.getInt("id");
			int StuID = rs.getInt("student_id");
			String mon1 = rs.getString("mon1");
			String mon2 = rs.getString("mon2");
			String mon3 = rs.getString("mon3");
			String mon4 = rs.getString("mon4");
			String mon5 = rs.getString("mon5");
			String tue1 = rs.getString("tue1");
			String tue2 = rs.getString("tue2");
			String tue3 = rs.getString("tue3");
			String tue4 = rs.getString("tue4");
			String tue5 = rs.getString("tue5");
			String wed1 = rs.getString("wed1");
			String wed2 = rs.getString("wed2");
			String wed3 = rs.getString("wed3");
			String wed4 = rs.getString("wed4");
			String wed5 = rs.getString("wed5");
			String thu1 = rs.getString("thu1");
			String thu2 = rs.getString("thu2");
			String thu3 = rs.getString("thu3");
			String thu4 = rs.getString("thu4");
			String thu5 = rs.getString("thu5");
			String fri1 = rs.getString("fri1");
			String fri2 = rs.getString("fri2");
			String fri3 = rs.getString("fri3");
			String fri4 = rs.getString("fri4");
			String fri5 = rs.getString("fri5");
			String mon1k = rs.getString("mon1k");
			String mon2k = rs.getString("mon2k");
			String mon3k = rs.getString("mon3k");
			String mon4k = rs.getString("mon4k");
			String mon5k = rs.getString("mon5k");
			String tue1k = rs.getString("tue1k");
			String tue2k = rs.getString("tue2k");
			String tue3k = rs.getString("tue3k");
			String tue4k = rs.getString("tue4k");
			String tue5k = rs.getString("tue5k");
			String wed1k = rs.getString("wed1k");
			String wed2k = rs.getString("wed2k");
			String wed3k = rs.getString("wed3k");
			String wed4k = rs.getString("wed4k");
			String wed5k = rs.getString("wed5k");
			String thu1k = rs.getString("thu1k");
			String thu2k = rs.getString("thu2k");
			String thu3k = rs.getString("thu3k");
			String thu4k = rs.getString("thu4k");
			String thu5k = rs.getString("thu5k");
			String fri1k = rs.getString("fri1k");
			String fri2k = rs.getString("fri2k");
			String fri3k = rs.getString("fri3k");
			String fri4k = rs.getString("fri4k");
			String fri5k = rs.getString("fri5k");

			timetable StuTT = new timetable(StuID,mon1,mon2,mon3,mon4,mon5,tue1,tue2,tue3,tue4,tue5,wed1,wed2,wed3,wed4,wed5,thu1,thu2,thu3,thu4,thu5,fri1,fri2,fri3,fri4,fri5,mon1k,mon2k,mon3k,mon4k,mon5k,tue1k,tue2k,tue3k,tue4k,tue5k,wed1k,wed2k,wed3k,wed4k,wed5k,thu1k,thu2k,thu3k,thu4k,thu5k,fri1k,fri2k,fri3k,fri4k,fri5k);

			return StuTT;

		}catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
}
