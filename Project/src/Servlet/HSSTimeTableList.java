package Servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Lesson;
import beans.University;
import beans.studentDateBeans;
import dao.LessonDAO;
import dao.studentDAO;
import dao.universityDAO;

/**
 * Servlet implementation class HSSTimeTableList
 */
@WebServlet("/HSSTimeTableList")
public class HSSTimeTableList extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public HSSTimeTableList() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String UniID = request.getParameter("UniID");

		List<studentDateBeans> StudentsList = studentDAO.Search(UniID);
		University university =  universityDAO.Search(UniID);
		try {
			List<Lesson> SearchListL = LessonDAO.LessonInfo(UniID);

		request.setAttribute("university", university);
		request.setAttribute("StudentsList",StudentsList);
		request.setAttribute("SearchListL",SearchListL);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/HSS-TimeTableList.jsp");
		dispatcher.forward(request, response);

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
