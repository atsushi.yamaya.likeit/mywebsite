package Servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Lesson;
import beans.Review;
import beans.University;
import beans.studentDateBeans;
import dao.LessonDAO;
import dao.reviewDAO;
import dao.studentDAO;
import dao.universityDAO;

/**
 * Servlet implementation class ReviewSet
 */
@WebServlet("/ReviewSet")
public class ReviewSet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ReviewSet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String review = request.getParameter("review");
		String impress = request.getParameter("impress");
		String LeID = request.getParameter("LeID");
		String UniID = request.getParameter("UniID");
		String StuID = request.getParameter("StuID");

		try {
			reviewDAO.Set(LeID,review,impress,StuID);


			Lesson lesson = LessonDAO.Info(LeID);
			studentDateBeans student = studentDAO.USInfo(StuID);
			University university =  universityDAO.Search(UniID);
			List<Review> ReviewList = reviewDAO.Search(LeID);
			double ReviewAve = reviewDAO.ave(LeID);

			request.setAttribute("lesson", lesson);
			request.setAttribute("university", university);
			request.setAttribute("student", student);
			request.setAttribute("ReviewList", ReviewList);
			request.setAttribute("ReviewAve", ReviewAve);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/LessonReview.jsp");
			dispatcher.forward(request, response);

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
