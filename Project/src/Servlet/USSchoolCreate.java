package Servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.University;
import dao.universityDAO;

/**
 * Servlet implementation class USSchoolCreate
 */
@WebServlet("/USSchoolCreate")
public class USSchoolCreate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public USSchoolCreate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/US-SchoolCreate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		request.setCharacterEncoding("UTF-8");

		String name = request.getParameter("name");

		if(name == null || name.equals("")) {
			request.setAttribute("errMsg","空欄を埋めてください");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/US-SchoolCreate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		University Check = universityDAO.Check(name);

		if(Check != null) {
			request.setAttribute("errMsg","入力された学校はすでに存在しています");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/US-SchoolCreate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		universityDAO.Create(name);

		response.sendRedirect("TopPage");


	}

}
