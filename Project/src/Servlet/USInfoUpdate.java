package Servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.University;
import beans.studentDateBeans;
import beans.timetable;
import dao.studentDAO;
import dao.timetableDAO;
import dao.universityDAO;

/**
 * Servlet implementation class USInfoUpdate
 */
@WebServlet("/USInfoUpdate")
public class USInfoUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public USInfoUpdate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String UniID = request.getParameter("UniID");
		String StuID = request.getParameter("StuID");

		studentDateBeans student = studentDAO.USInfo(StuID);
		University university =  universityDAO.Search(UniID);

		request.setAttribute("university", university);
		request.setAttribute("student", student);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/US-InfoUpdate.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		request.setCharacterEncoding("UTF-8");

		String UniID = request.getParameter("UniID");
		String StuID = request.getParameter("StuID");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		String passwordC = request.getParameter("passwordC");

		if(name.equals("") || password.equals("") || passwordC.equals("")) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			studentDateBeans student = studentDAO.USInfo(StuID);
			University university =  universityDAO.Search(UniID);

			request.setAttribute("university", university);
			request.setAttribute("student", student);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/US-InfoUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}else if(!(password.equals(passwordC))) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			studentDateBeans student = studentDAO.USInfo(StuID);
			University university =  universityDAO.Search(UniID);

			request.setAttribute("university", university);
			request.setAttribute("student", student);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/US-InfoUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		studentDAO.InfoUpdate(StuID,password,name);

		University university =  universityDAO.Search(UniID);
		studentDateBeans student =  studentDAO.USInfo(StuID);
		timetable StuTT = timetableDAO.Search(StuID);

		request.setAttribute("university", university);
		request.setAttribute("student", student);
		request.setAttribute("StuTT", StuTT);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/US-TimeTable.jsp");
		dispatcher.forward(request, response);

	}

}
