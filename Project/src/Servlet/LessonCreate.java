package Servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Lesson;
import beans.University;
import beans.studentDateBeans;
import beans.timetable;
import dao.LessonDAO;
import dao.studentDAO;
import dao.timetableDAO;
import dao.universityDAO;

/**
 * Servlet implementation class LessonCreate
 */
@WebServlet("/LessonCreate")
public class LessonCreate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public LessonCreate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String UniID = request.getParameter("UniID");
		String StuID = request.getParameter("StuID");

		studentDateBeans student = studentDAO.USInfo(StuID);
		University university =  universityDAO.Search(UniID);

		request.setAttribute("student", student);
		request.setAttribute("university", university);

		RequestDispatcher dispacther = request.getRequestDispatcher("WEB-INF/jsp/LessonCreate.jsp");
		dispacther.forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		request.setCharacterEncoding("UTF-8");

		String UniID = request.getParameter("UniID");
		String LessonN = request.getParameter("LessonN");
		String professor = request.getParameter("professor");
		String Season = request.getParameter("season");
		String Day = request.getParameter("day");
		String Time = request.getParameter("time");
		String Credit = request.getParameter("credit");
		String Place = request.getParameter("place");
		String Content = request.getParameter("content");
		String Test = request.getParameter("test");

		if(LessonN.equals("") || Season.equals("") || Day.equals("") || Time.equals("") || Credit.equals("")) {
			request.setAttribute("errMsg", "入力された内容が正しくありません");

			String StuID = request.getParameter("StuID");

			studentDateBeans student = studentDAO.USInfo(StuID);
			University university =  universityDAO.Search(UniID);

			request.setAttribute("university", university);
			request.setAttribute("student", student);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/LessonCreate.jsp");
			dispatcher.forward(request,response);
			return;

		}

		LessonDAO.LessonCreate(UniID,LessonN,professor,Season,Day,Time,Credit,Place,Content,Test);


		String StuID = request.getParameter("StuID");

		studentDateBeans student = studentDAO.USInfo(StuID);
		University university =  universityDAO.Search(UniID);
		timetable StuTT = timetableDAO.Search(StuID);
		List<Lesson> LessonList;
		try {
			LessonList = LessonDAO.LessonList(UniID);

		request.setAttribute("university", university);
		request.setAttribute("student", student);
		request.setAttribute("LessonList",LessonList);
		request.setAttribute("StuTT", StuTT);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/US-TTSet.jsp");
		dispatcher.forward(request,response);

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}


	}

}
