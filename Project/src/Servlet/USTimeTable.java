package Servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.University;
import beans.studentDateBeans;
import beans.timetable;
import dao.LessonDAO;
import dao.studentDAO;
import dao.timetableDAO;
import dao.universityDAO;

/**
 * Servlet implementation class USTimeTable
 */
@WebServlet("/USTimeTable")
public class USTimeTable extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public USTimeTable() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("login_id");
		String password = request.getParameter("password");
		String UniId = request.getParameter("UniID");

		studentDateBeans student;
		student = studentDAO.LoginSchool(loginId,password,UniId);

		if(student == null) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			University university =  universityDAO.Search(UniId);

			request.setAttribute("university", university);

			RequestDispatcher dispacther = request.getRequestDispatcher("WEB-INF/jsp/US-Login.jsp");
			dispacther.forward(request,response);
		}


		University university =  universityDAO.Search(UniId);
		timetable StuTT = LessonDAO.LoginSearch(student.getId());


		request.setAttribute("university", university);
		request.setAttribute("student",student);
		request.setAttribute("StuTT",StuTT);

		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/US-TimeTable.jsp");
		dispatcher.forward(request,response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String StuID = request.getParameter("StuID");
		String UniID = request.getParameter("UniID");

		if(StuID == "" || UniID == "") {

			List<University> universityList = universityDAO.finduniList();

			request.setAttribute("universityList", universityList);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/TopPage.jsp");
			dispatcher.forward(request, response);
		}

		studentDateBeans student = studentDAO.USInfo(StuID);
		University university =  universityDAO.Search(UniID);
		timetable StuTT = timetableDAO.Search(StuID);

		request.setAttribute("university", university);
		request.setAttribute("student", student);
		request.setAttribute("StuTT", StuTT);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/US-TimeTable.jsp");
		dispatcher.forward(request, response);


	}

}
