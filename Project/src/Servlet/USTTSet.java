package Servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Lesson;
import beans.University;
import beans.studentDateBeans;
import beans.timetable;
import dao.LessonDAO;
import dao.studentDAO;
import dao.timetableDAO;
import dao.universityDAO;

/**
 * Servlet implementation class USTTSet
 */
@WebServlet("/USTTSet")
public class USTTSet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public USTTSet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String StuID = request.getParameter("StuID");
		String UniID = request.getParameter("UniID");

		University university =  universityDAO.Search(UniID);
		studentDateBeans student = studentDAO.USInfo(StuID);

		timetable StuTT = timetableDAO.Search(StuID);
		try {
			List<Lesson> LessonList = LessonDAO.LessonList(UniID);


		request.setAttribute("university", university);
		request.setAttribute("student", student);
		request.setAttribute("LessonList",LessonList);
		request.setAttribute("StuTT", StuTT);


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/US-TTSet.jsp");
		dispatcher.forward(request, response);

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		request.setCharacterEncoding("UTF-8");

		String StuID = request.getParameter("StuID");
		String UniID = request.getParameter("UniID");
		String mon1 = request.getParameter("mon1");
		String mon2 = request.getParameter("mon2");
		String mon3 = request.getParameter("mon3");
		String mon4 = request.getParameter("mon4");
		String mon5 = request.getParameter("mon5");
		String tue1 = request.getParameter("tue1");
		String tue2 = request.getParameter("tue2");
		String tue3 = request.getParameter("tue3");
		String tue4 = request.getParameter("tue4");
		String tue5 = request.getParameter("tue5");
		String wed1 = request.getParameter("wed1");
		String wed2 = request.getParameter("wed2");
		String wed3 = request.getParameter("wed3");
		String wed4 = request.getParameter("wed4");
		String wed5 = request.getParameter("wed5");
		String thu1 = request.getParameter("thu1");
		String thu2 = request.getParameter("thu2");
		String thu3 = request.getParameter("thu3");
		String thu4 = request.getParameter("thu4");
		String thu5 = request.getParameter("thu5");
		String fri1 = request.getParameter("fri1");
		String fri2 = request.getParameter("fri2");
		String fri3 = request.getParameter("fri3");
		String fri4 = request.getParameter("fri4");
		String fri5 = request.getParameter("fri5");
		String mon1k = request.getParameter("mon1k");
		String mon2k = request.getParameter("mon2k");
		String mon3k = request.getParameter("mon3k");
		String mon4k = request.getParameter("mon4k");
		String mon5k = request.getParameter("mon5k");
		String tue1k = request.getParameter("tue1k");
		String tue2k = request.getParameter("tue2k");
		String tue3k = request.getParameter("tue3k");
		String tue4k = request.getParameter("tue4k");
		String tue5k = request.getParameter("tue5k");
		String wed1k = request.getParameter("wed1k");
		String wed2k = request.getParameter("wed2k");
		String wed3k = request.getParameter("wed3k");
		String wed4k = request.getParameter("wed4k");
		String wed5k = request.getParameter("wed5k");
		String thu1k = request.getParameter("thu1k");
		String thu2k = request.getParameter("thu2k");
		String thu3k = request.getParameter("thu3k");
		String thu4k = request.getParameter("thu4k");
		String thu5k = request.getParameter("thu5k");
		String fri1k = request.getParameter("fri1k");
		String fri2k = request.getParameter("fri2k");
		String fri3k = request.getParameter("fri3k");
		String fri4k = request.getParameter("fri4k");
		String fri5k = request.getParameter("fri5k");

		studentDateBeans stuDateCheck = studentDAO.Check(StuID);

		if(stuDateCheck == null || stuDateCheck.equals("")){
			timetableDAO.create(StuID,mon1,mon2,mon3,mon4,mon5,tue1,tue2,tue3,tue4,tue5,wed1,wed2,wed3,wed4,wed5,thu1,thu2,thu3,thu4,thu5,fri1,fri2,fri3,fri4,fri5,mon1k,mon2k,mon3k,mon4k,mon5k,tue1k,tue2k,tue3k,tue4k,tue5k,wed1k,wed2k,wed3k,wed4k,wed5k,thu1k,thu2k,thu3k,thu4k,thu5k,fri1k,fri2k,fri3k,fri4k,fri5k);
		}else {
			timetableDAO.update(StuID,mon1,mon2,mon3,mon4,mon5,tue1,tue2,tue3,tue4,tue5,wed1,wed2,wed3,wed4,wed5,thu1,thu2,thu3,thu4,thu5,fri1,fri2,fri3,fri4,fri5,mon1k,mon2k,mon3k,mon4k,mon5k,tue1k,tue2k,tue3k,tue4k,tue5k,wed1k,wed2k,wed3k,wed4k,wed5k,thu1k,thu2k,thu3k,thu4k,thu5k,fri1k,fri2k,fri3k,fri4k,fri5k);
		}

		studentDateBeans student = studentDAO.USInfo(StuID);
		University university =  universityDAO.Search(UniID);

		timetable StuTT = timetableDAO.Search(StuID);


		request.setAttribute("university", university);
		request.setAttribute("student", student);
		request.setAttribute("StuTT", StuTT);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/US-TimeTable.jsp");
		dispatcher.forward(request,response);

	}

}
