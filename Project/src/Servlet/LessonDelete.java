package Servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.University;
import beans.studentDateBeans;
import beans.timetable;
import dao.LessonDAO;
import dao.studentDAO;
import dao.timetableDAO;
import dao.universityDAO;

/**
 * Servlet implementation class LessonDelete
 */
@WebServlet("/LessonDelete")
public class LessonDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public LessonDelete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String LeID = request.getParameter("LeID");
		String UniID = request.getParameter("UniID");
		String StuID = request.getParameter("StuID");

		studentDateBeans student = studentDAO.USInfo(StuID);
		University university =  universityDAO.Search(UniID);
		try {
			LessonDAO.LessonDelete(LeID);

			timetable StuTT = timetableDAO.Search(StuID);

		request.setAttribute("university", university);
		request.setAttribute("student", student);
		request.setAttribute("StuTT", StuTT);

		RequestDispatcher dispacther = request.getRequestDispatcher("WEB-INF/jsp/US-TimeTable.jsp");
		dispacther.forward(request,response);


		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
