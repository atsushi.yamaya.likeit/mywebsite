package Servlet;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.University;
import beans.studentDateBeans;
import dao.studentDAO;
import dao.universityDAO;

/**
 * Servlet implementation class USLogin
 */
@WebServlet("/USLogin")
public class USLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public USLogin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		String name = request.getParameter("name");

			University university;
			try {
				university = universityDAO.Login(name);

				if(university == null || university.equals("")) {
					response.sendRedirect("TopPage");
					return;
				}

			request.setAttribute("university",university);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/US-Login.jsp");
			dispatcher.forward(request, response);

			} catch (NoSuchAlgorithmException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}

	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("login_id");
		String password = request.getParameter("password");
		String UniId = request.getParameter("UniID");

		studentDateBeans student = studentDAO.LoginSchool(loginId,password,UniId);


			if(student == null) {
				request.setAttribute("errMsg", "入力された内容は正しくありません");
				University university =  universityDAO.Search(UniId);
				request.setAttribute("university", university);
				RequestDispatcher dispacther = request.getRequestDispatcher("WEB-INF/jsp/US-Login.jsp");
				dispacther.forward(request,response);
			}


			University university =  universityDAO.Search(UniId);

			request.setAttribute("university", university);
			request.setAttribute("student",student);

			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/US-TimeTable.jsp");
			dispatcher.forward(request,response);

	}
}


