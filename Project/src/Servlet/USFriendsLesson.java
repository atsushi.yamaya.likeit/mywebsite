package Servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.University;
import beans.studentDateBeans;
import beans.timetable;
import dao.studentDAO;
import dao.timetableDAO;
import dao.universityDAO;

/**
 * Servlet implementation class USFriendsLesson
 */
@WebServlet("/USFriendsLesson")
public class USFriendsLesson extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public USFriendsLesson() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String UniID = request.getParameter("UniID");
		String StuID = request.getParameter("StuID");
		String FreID = request.getParameter("FreID");

		studentDateBeans student = studentDAO.USInfo(StuID);
		University university =  universityDAO.Search(UniID);
		studentDateBeans friend = studentDAO.USInfo(FreID);

		timetable StuTT = timetableDAO.Search(FreID);

		request.setAttribute("university", university);
		request.setAttribute("student", student);
		request.setAttribute("friend",friend);
		request.setAttribute("StuTT", StuTT);

		RequestDispatcher dispacther = request.getRequestDispatcher("WEB-INF/jsp/US-Friend'sLesson.jsp");
		dispacther.forward(request,response);


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
