package Servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.University;
import beans.favorite;
import beans.studentDateBeans;
import dao.favoriteDAO;
import dao.studentDAO;
import dao.universityDAO;

/**
 * Servlet implementation class USfavoList
 */
@WebServlet("/USfavoList")
public class USfavoList extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public USfavoList() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String UniID = request.getParameter("UniID");
		String StuID = request.getParameter("StuID");

		List<favorite> favoList = favoriteDAO.Search(StuID);
		studentDateBeans student = studentDAO.USInfo(StuID);
		University university =  universityDAO.Search(UniID);

		request.setAttribute("university", university);
		request.setAttribute("student", student);
		request.setAttribute("favoList", favoList);

		RequestDispatcher dispacther = request.getRequestDispatcher("WEB-INF/jsp/US-Favorite.jsp");
		dispacther.forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
