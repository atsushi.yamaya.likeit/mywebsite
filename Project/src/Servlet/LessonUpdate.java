package Servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Lesson;
import beans.University;
import beans.studentDateBeans;
import dao.LessonDAO;
import dao.studentDAO;
import dao.universityDAO;

/**
 * Servlet implementation class LessonUpdate
 */
@WebServlet("/LessonUpdate")
public class LessonUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public LessonUpdate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String LeID = request.getParameter("LeID");
		String UniID = request.getParameter("UniID");
		String StuID = request.getParameter("StuID");

		Lesson lesson = LessonDAO.Info(LeID);
		studentDateBeans student = studentDAO.USInfo(StuID);
		University university =  universityDAO.Search(UniID);

		request.setAttribute("lesson",lesson);
		request.setAttribute("university", university);
		request.setAttribute("student", student);


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/LessonUpdate.jsp");
		dispatcher.forward(request,response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub


		request.setCharacterEncoding("UTF-8");

		String LeID = request.getParameter("LeID");
		String StuID = request.getParameter("StuID");
		String UniID = request.getParameter("UniID");
		String LessonN = request.getParameter("LessonN");
		String professor = request.getParameter("professor");
		String Season = request.getParameter("season");
		String Day = request.getParameter("day");
		String Time = request.getParameter("time");
		String Credit = request.getParameter("credit");
		String Place = request.getParameter("place");
		String Content = request.getParameter("content");
		String Test = request.getParameter("test");

		if(LessonN.equals("") || Season.equals("") || Day.equals("") || Time.equals("") || Credit.equals("")) {
			request.setAttribute("errMsg", "空欄を埋めてください");

			studentDateBeans student = studentDAO.USInfo(StuID);
			University university =  universityDAO.Search(UniID);
			Lesson lesson = LessonDAO.Info(LeID);

			request.setAttribute("university", university);
			request.setAttribute("student", student);
			request.setAttribute("lesson", lesson);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/LessonInfo.jsp");
			dispatcher.forward(request,response);
			return;

		}

		LessonDAO.LessonUpdate(LeID,LessonN,professor,Season,Day,Time,Credit,Place,Content,Test);

		Lesson lesson = LessonDAO.Info(LeID);
		studentDateBeans student = studentDAO.USInfo(StuID);
		University university =  universityDAO.Search(UniID);

		request.setAttribute("lesson", lesson);
		request.setAttribute("university", university);
		request.setAttribute("student", student);


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/LessonInfo.jsp");
		dispatcher.forward(request,response);


	}

}
