package Servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.University;
import beans.favorite;
import beans.studentDateBeans;
import dao.favoriteDAO;
import dao.studentDAO;
import dao.universityDAO;

/**
 * Servlet implementation class USFavorite
 */
@WebServlet("/USFavorite")
public class USFavorite extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public USFavorite() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String UniID = request.getParameter("UniID");
		String StuID = request.getParameter("StuID");
		String FreID = request.getParameter("FreID");

		studentDateBeans student = studentDAO.USInfo(StuID);
		University university =  universityDAO.Search(UniID);
		try {
			favoriteDAO.Set(StuID,FreID);

			List<favorite> favoList = favoriteDAO.Search(StuID);


		request.setAttribute("university", university);
		request.setAttribute("student", student);
		request.setAttribute("favoList", favoList);

		RequestDispatcher dispacther = request.getRequestDispatcher("WEB-INF/jsp/US-Favorite.jsp");
		dispacther.forward(request,response);

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
