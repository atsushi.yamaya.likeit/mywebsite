package Servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.University;
import beans.studentDateBeans;
import dao.studentDAO;
import dao.universityDAO;


/**
 * Servlet implementation class USSearchResultF
 */
@WebServlet("/USSearchResultF")
public class USSearchResultF extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public USSearchResultF() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String StuID = request.getParameter("StuID");
		String UniID = request.getParameter("UniID");
		String USName = request.getParameter("USName");

		try {

				studentDateBeans student = studentDAO.USInfo(StuID);
				University university =  universityDAO.Search(UniID);
				List<studentDateBeans> SearchListF = studentDAO.SearchFriend(UniID,USName);


			request.setAttribute("SearchListF",SearchListF);
			request.setAttribute("university", university);
			request.setAttribute("student", student);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/US-SearchResultF.jsp");
			dispatcher.forward(request,response);

			} catch (SQLException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
