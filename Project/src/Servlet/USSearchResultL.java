package Servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Lesson;
import beans.University;
import beans.studentDateBeans;
import dao.LessonDAO;
import dao.studentDAO;
import dao.universityDAO;

/**
 * Servlet implementation class USSearchResultL
 */
@WebServlet("/USSearchResultL")
public class USSearchResultL extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public USSearchResultL() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		request.setCharacterEncoding("UTF-8");

		String UniID = request.getParameter("UniID");
		String lesson = request.getParameter("lessonN");
		String StuID = request.getParameter("StuID");

		List<Lesson> SearchListL = LessonDAO.SearchLesson(UniID,lesson);


		if(SearchListL == null || SearchListL.equals("") || SearchListL.size() == 0) {
			request.setAttribute("errMsg", "入力された内容が見つかりません");

			studentDateBeans student = studentDAO.USInfo(StuID);
			University university =  universityDAO.Search(UniID);

			request.setAttribute("university", university);
			request.setAttribute("student", student);

			RequestDispatcher dispacther = request.getRequestDispatcher("WEB-INF/jsp/US-TimeTable.jsp");
			dispacther.forward(request,response);
			return;
		}

		studentDateBeans student = studentDAO.USInfo(StuID);
		University university =  universityDAO.Search(UniID);

		request.setAttribute("university", university);
		request.setAttribute("student", student);
		request.setAttribute("SearchListL", SearchListL);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/US-SearchResultL.jsp");
		dispatcher.forward(request, response);


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

	}

}
