package Servlet;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.University;
import beans.studentDateBeans;
import dao.studentDAO;
import dao.universityDAO;

/**
 * Servlet implementation class USLoginInfoSet
 */
@WebServlet("/USLoginInfoSet")
public class USLoginInfoSet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public USLoginInfoSet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String UniID = request.getParameter("UniID");
		University university =  universityDAO.Search(UniID);
		request.setAttribute("university", university);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/US-LoginInfoSet.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		request.setCharacterEncoding("UTF-8");

		String UniID = request.getParameter("UniID");

		String name = request.getParameter("name");
		String loginId = request.getParameter("login_id");
		String password = request.getParameter("password");
		String passwordC = request.getParameter("passwordC");


		String str = "";
		if(name == "" || loginId == "" || password == "" || passwordC == "" || str.equals("name") || str.equals("loginId") || str.equals("password") || str.equals("passwordC")){
			request.setAttribute("errMsg","空欄を全て埋めてください");

			University university =  universityDAO.Search(UniID);
			request.setAttribute("university", university);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/US-LoginInfoSet.jsp");
			dispatcher.forward(request, response);
			return ;

		}else if(!(password.equals(passwordC))){
			request.setAttribute("errMsg","パスワードと確認用パスワードが異なっています");

			University university =  universityDAO.Search(UniID);
			request.setAttribute("university", university);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/US-LoginInfoSet.jsp");
			dispatcher.forward(request, response);

			return ;
		}

		studentDateBeans Check = studentDAO.LoginCheck(loginId);
		if(Check != null) {
			request.setAttribute("errMsg","入力されたログインIDは使われております");

			University university =  universityDAO.Search(UniID);
			request.setAttribute("university", university);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/US-LoginInfoSet.jsp");
			dispatcher.forward(request, response);

			return ;

		}

		try {

			studentDAO.Set(loginId,password,name,UniID);
			University university =  universityDAO.Search(UniID);

			request.setAttribute("university", university);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/US-Login.jsp");
			dispatcher.forward(request, response);

		} catch (NoSuchAlgorithmException e) {

			e.printStackTrace();
		}
	}
}
