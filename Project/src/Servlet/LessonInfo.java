package Servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Lesson;
import beans.University;
import beans.studentDateBeans;
import dao.LessonDAO;
import dao.studentDAO;
import dao.universityDAO;

/**
 * Servlet implementation class LessonInfo
 */
@WebServlet("/LessonInfo")
public class LessonInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public LessonInfo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

			String LeID = request.getParameter("LeID");
			String StuID = request.getParameter("StuID");
			String UniID = request.getParameter("UniID");
			String LeName = request.getParameter("LeName");


			if(LeID == "" || LeID == null) {
				Lesson lesson = LessonDAO.NameSearch(UniID,LeName);
				request.setAttribute("lesson", lesson);

				studentDateBeans student = studentDAO.USInfo(StuID);
				University university =  universityDAO.Search(UniID);

				request.setAttribute("university", university);
				request.setAttribute("student", student);

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/LessonInfo.jsp");
				dispatcher.forward(request,response);
				return;
			}else {
				Lesson lesson = LessonDAO.Info(LeID);
				request.setAttribute("lesson", lesson);

				studentDateBeans student = studentDAO.USInfo(StuID);
				University university =  universityDAO.Search(UniID);

				request.setAttribute("university", university);
				request.setAttribute("student", student);

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/LessonInfo.jsp");
				dispatcher.forward(request,response);
				return ;


			}



	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
